import {
  INQUIRY_SAVE_SUCCESS,
  INQUIRY_SAVE_FAILURE,
  ADD_VALUE_IRRATABLEBOWELSYNDROM,
  ADD_VALUE_CELIACDISEASE,
  ADD_VALUE_IRRATABLEBOWELDISEASE,
  ADD_VALUE_OTHERCONDITION,
  ADD_VALUE_SLIDER,
  LOAD_INQUIRY_SUCCESS,
  LOAD_INQUIRY_FAILURE
} from "../actions/types"

export const INITIAL_STATE = {
  inqury: null,
  success: null,
  message: null,
  value: 10,
  irratableBowelSyndrome: false,
  celiacDisease: false,
  irritableBowelDiease: false,
  otherConditionValue: "",
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case ADD_VALUE_IRRATABLEBOWELSYNDROM:
      return { ...state, irratableBowelSyndrome: action.payload }
    case ADD_VALUE_CELIACDISEASE:
      return { ...state, celiacDisease: action.payload }
    case ADD_VALUE_IRRATABLEBOWELDISEASE:
      return { ...state, irritableBowelDiease: action.payload }
    case ADD_VALUE_OTHERCONDITION:
      return { ...state, otherConditionValue: action.payload }
    case ADD_VALUE_SLIDER:
      return { ...state, value: action.payload }
    case INQUIRY_SAVE_SUCCESS:
      return { ...state, success: action.payload }
    case INQUIRY_SAVE_FAILURE:
      return { ...state, message: action.payload }
    case LOAD_INQUIRY_SUCCESS:
      return { ...state, inqury: action.payload }
    case LOAD_INQUIRY_FAILURE:
      return { ...state, message: action.payload }

    default:
      return state
  }
}
