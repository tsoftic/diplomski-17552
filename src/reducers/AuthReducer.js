import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  ADD_USER_ID,
  REMOVE_USER_ID,
  RESETPASS_USER_SUCCESS,
  RESETPASS_USER_FAILURE,
  RESETMESSAGE_ERROR_MESSAGE,
  LOAD_PROFILE_SETTINGS,
  LOAD_PROFILE_FAILURE,
  UPDATE_USER_PROFILE,
  UPDATE_EMAIL_PREFERENCES,
  UPDATE_USER_PROFILE_FAILURE,
  UPDATE_EMAIL_PREFERENCES_FAILURE,
  LOAD_EMAIL_PREFERENCES_FAILURE,
  LOAD_EMAIL_PREFERENCES,
  UPDATED_PREFERENCE,
  CHANGE_PASSWORD_FAILURE,
  CHANGE_PASSWORD_SUCCESS
} from '../actions/types'
import { gender } from '../utilities/enum'

export const INITIAL_STATE = {
  bio: false,
  user: null,
  uid: null,
  message: null,
  linkSent: false,
  email: null,
  displayName: null,
  isSuccess: null,
  profileSettings: {
    firstName: '',
    lastName: '',
    gender: gender.Unknown
  },
  emailPreferences: {
    email: '',
    allowNotifications: false,
    newsletters: false,
    dietChallengeStatus: false
  }
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case LOGIN_USER_SUCCESS:
      return { ...state, user: action.payload }

    case LOGIN_USER_FAILURE:
      return { ...state, message: action.payload }

    case REGISTER_USER_SUCCESS:
      return { ...state, user: action.payload }

    case REGISTER_USER_FAILURE:
      return { ...state, message: action.payload }

    case ADD_USER_ID: {
      return {
        ...state, uid: action.payload.uid,
        displayName: ( action.payload.displayName ) ? action.payload.displayName : 'John Doe',
        email: action.payload.email
      }
    }
    case REMOVE_USER_ID:
      return { ...state, uid: action.payload }

    case RESETPASS_USER_SUCCESS:
      return { ...state, linkSent: action.payload }

    case RESETPASS_USER_FAILURE:
    case LOAD_PROFILE_FAILURE:
    case CHANGE_PASSWORD_FAILURE:
      return { ...state, message: action.payload }

    case RESETMESSAGE_ERROR_MESSAGE:
      return { ...state, message: action.payload }

    case LOAD_PROFILE_SETTINGS:
      return { ...state, profileSettings: action.payload }

    case UPDATE_USER_PROFILE:
      return { ...state, message: action.payload }
    case UPDATE_EMAIL_PREFERENCES:
      return { ...state, message: action.payload }
    case CHANGE_PASSWORD_SUCCESS:
      return { ...state, message: action.payload }

    case UPDATE_USER_PROFILE_FAILURE:
    case UPDATE_EMAIL_PREFERENCES_FAILURE:
      return { ...state, message: action.payload }
    case LOAD_EMAIL_PREFERENCES_FAILURE:
      return { ...state, message: action.payload }

    case LOAD_EMAIL_PREFERENCES:
      return { ...state, emailPreferences: action.payload }
    case UPDATED_PREFERENCE:
      return {
        ...state, emailPreferences: {
          ...state.emailPreferences,
          [ action.payload.title ]: action.payload.value
        }
      }
    default:
      return state
  }
}
