import {
  TEST_REDUCER_ACTION,
} from '../actions/types'

export const INITIAL_STATE = {
  data: 'INITIAL_STATE',
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case TEST_REDUCER_ACTION:
      return { ...state, data: action.payload }

    default:
      return state
  }
}
