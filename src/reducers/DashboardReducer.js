import { TRIGGERS_SAVE_SUCCESS, TRIGGERS_SAVE_FAILURE, ADD_TRIGGER_ID, SYMPTOMS_SAVE_SUCCESS, SYMPTOMS_SAVE_FAILURE, LATEST_TRIGGER, UPDATED_TRIGGER } from "../actions/types"

export const INITIAL_STATE = {
  success: null,
  message: null,
  triggerId: null,
  uid: null,
  trigger: {
    pasta: false,
    meat: false,
    seafood: false,
    diaryProducts: false,
    vegetables: false,
    fruits: false
  }
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case TRIGGERS_SAVE_SUCCESS:
      return {
        ...state,
        success: action.payload
      }
    case TRIGGERS_SAVE_FAILURE:
      return {
        ...state,
        message: action.payload
      }
    case ADD_TRIGGER_ID:
      return {
        ...state,
        triggerId: action.payload
      }
    case SYMPTOMS_SAVE_SUCCESS:
      return {
        ...state, success: action.payload
      }
    case SYMPTOMS_SAVE_FAILURE:
      return {
        ...state, message: action.payload
      }
    case LATEST_TRIGGER:
      return {
        ...state, trigger: action.payload
      }
    case UPDATED_TRIGGER:
      return {
        ...state, trigger: {
          ...state.trigger,
          [ action.payload.title ]: action.payload.value
        } }
    default:
      return state
  }
}
