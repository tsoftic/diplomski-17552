import {
  SELECT_MEAL,
} from '../actions/types'

const INITIAL_STATE = {
  meal: null,
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case SELECT_MEAL:
      return { ...state, meal: action.payload }

    default:
      return state
  }
}
