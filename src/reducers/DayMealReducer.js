import {
  GET_CHALLENGEWEEK,
  GET_ALLCHALLENGEWEEKS
} from "../actions/types"

export const INITIAL_STATE = {
  _id: null,
  day: 0,
  challengeWeek: null
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
    case GET_CHALLENGEWEEK:
      return { ...state, challengeWeek: action.payload }
    case GET_ALLCHALLENGEWEEKS:
      return{ ...state, allChallengeWeeks: action.payload }
    default:
      return state
  }
}
