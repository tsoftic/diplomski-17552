import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
// import { reducer as formReducer } from 'redux-form'

import TestReducer from './TestReducer'
import AuthReducer from './AuthReducer'
import InquiryReducer from './InquiryReducer'
import DashboardReducer from './DashboardReducer'
import DayMealReducer from './DayMealReducer'
import SingleMealReducer from './SingleMealReducer'

export default combineReducers( {
  router: routerReducer,
  testState: TestReducer,
  authState: AuthReducer,
  inquiryState: InquiryReducer,
  dashboardState: DashboardReducer,
  dayMealState: DayMealReducer,
  singleMealState: SingleMealReducer,
} )
