import React, { Component } from 'react'
import firebase from 'firebase'
import logo from '../../photos/carolaLogo.png'
import googlelogo from '../../photos/googlelogo.png'
import FooterLogin from '../common/FooterLogin'
import '../../scss/Register.css'
import '../../scss/Common.css'

class Register extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
    }

    this.handleChange = this.handleChange.bind( this )
    this.handleSubmit = this.handleSubmit.bind( this )

    this.goToLogin = this.goToLogin.bind( this )
    this.redirectToOnboarding = this.redirectToOnboarding.bind( this )
  }

  componentWillMount() {
    this.props.registerProps.resetMessage()
  }

  goToLogin() {
    this.props.registerProps.history.push( "/login" )
  }

  handleChange( event ) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState( {
      [ name ]: value,
    } )
  }

  redirectToOnboarding() {
    return this
      .props
      .registerProps
      .history
      .push( "/onboarding" )
  }

  handleSubmit( event ) {
    let payload = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      password: this.state.password
    }
    this.props.registerProps.registerUser( payload, this.redirectToOnboarding )
    event.preventDefault()
  }

  renderRegister() {
    return (
      <div className="registerDiv font">
        <div className="register">
          <img className="photo" src={ logo } alt="photo"/>
          <label className="title">Create account</label>
          
          <p className="signInSocialMedia">SIGN UP WITH SOCIAL MEDIA</p>
          <button
            className="google"
            onClick={ () => this
              .props
              .registerProps
              .authenticate( new firebase.auth.GoogleAuthProvider(), this.redirectToOnboarding ) }
          >
            <img src={ googlelogo } alt="photo"/>
            <label className="pointer">GOOGLE</label>
          </button>
          <p className="signInEmail">SIGN UP WITH EMAIL</p>
          <form onSubmit={ this.handleSubmit } >
            <div className="inline">
              <input
                className="firstLastName placeholders font" style={ { marginRight: 2.5 } }
                type="text"
                name="firstName"
                placeholder="First Name"
                value={ this.state.firstName }
                onChange={ this.handleChange }
              />
              <br />
              <input
                className="firstLastName placeholders font" style={ { marginLeft: 2.5 } }
                type="text"
                name="lastName"
                placeholder="Last Name"
                value={ this.state.lastName }
                onChange={ this.handleChange }
              />
              <br />
            </div>
            <input
              className="Field_Shape placeholders font"
              type="text"
              name="email"
              placeholder="Email address"
              value={ this.state.email }
              onChange={ this.handleChange }
            />
            <br />
            <input
              className="Field_Shape placeholders font"
              type="password"
              name="password"
              placeholder="Password"
              value={ this.state.password }
              onChange={ this.handleChange }
            />
            <br />
            <input className="signIn pointer font" type="submit" name="submit" value="CREATE AN ACCOUNT" />
            {!this.props.registerProps.message === '' && (
              <div className="errorMessage">
                {this.props.registerProps.message}
              </div>
            )}
          </form>
          <div className="belowForm">
            <label>ALREADY HAVE AN ACCOUNT?</label>
            <a className="signUp" name="linkRegistracija" onClick={ this.goToLogin }>SIGN IN HERE</a>
          </div>
        </div>
        <FooterLogin />
      </div>
    )
  }

  // this will be in containers - only login should stay
  render() {
    return this.renderRegister()
  }
}

export default ( Register )
