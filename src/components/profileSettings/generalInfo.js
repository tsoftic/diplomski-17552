import React, { Component } from 'react'
import '../../scss/GeneralInfo.css'
import { BreadcrumbsSettings } from '../../components/common'
import { gender } from "../../utilities/enum"
import closeNotification from '../../photos/close-notification.png'

class GeneralInfo extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      uid: this.props.uid,
      gender: gender.Unknown,
      title: 'Edit General Information',
      firstName: '',
      lastName: '',
      male: false,
      female: false,
      statesFilled: false,
      closeClicked: false,
    }
    this.handleSubmit = this.handleSubmit.bind( this )
    this.handleChange = this.handleChange.bind( this )
    this.handleCheckboxChangeMale = this.handleCheckboxChangeMale.bind( this )
    this.handleCheckboxChangeFemale = this.handleCheckboxChangeFemale.bind( this )
    this.functionClicked = this.functionClicked.bind( this )
  }

  componentWillMount() {
    setTimeout( () => { this.props.loadProfile( this.props.uid ) }, 1000 )
  }

  componentWillReceiveProps( nextProps ) {
    if ( nextProps ) {
      this.setState( { uid: nextProps.uid } )

      if ( nextProps.profileSettings &&
        ( nextProps.profileSettings.firstName !== this.state.firstName
          || nextProps.profileSettings.lastName !== this.state.lastName
          || nextProps.profileSettings.gender !== this.state.gender ) ) {
        this.setState( {
          firstName: nextProps.profileSettings.firstName,
          lastName: nextProps.profileSettings.lastName,
          male: nextProps.profileSettings.gender === 'Male',
          female: nextProps.profileSettings.gender === 'Female',
          gender: nextProps.profileSettings.gender
        } )
      }
    }
  }

  handleCheckboxChangeMale( event ) {
    this.setState( { male: event.target.checked } )
    this.setState( { female: !event.target.checked } )
    this.state.male = true
    this.state.female = false
    this.state.statesFilledGender = ( this.state.male
      || this.state.female )
    this.state.statesFilled = ( this.state.statesFilledName
      && this.state.statesFilledGender )

    if ( ( this.state.male || this.state.female ) && this.state.firstName && this.state.lastName ) {
      this.setState( {
        statesFilled: true,
      } )
    }
  }

  handleCheckboxChangeFemale( event ) {
    this.setState( { female: event.target.checked } )
    this.setState( { male: !event.target.checked } )
    this.state.male = false
    this.state.female = true
    this.state.statesFilledGender = ( this.state.male
      || this.state.female )
    this.state.statesFilled = ( this.state.statesFilledName
      && this.state.statesFilledGender )

    if ( ( this.state.male || this.state.female ) && this.state.firstName && this.state.lastName ) {
      this.setState( {
        statesFilled: true,
      } )
    }
  }

  handleSubmit( event ) {
    this.setState( {
      closeClicked: false,
    } )
    let payload = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      gender: this.state.male ? gender.Male : ( this.state.female ? gender.Female : gender.Unknown ),
      uid: this.state.uid
    }
    event.preventDefault()
    this.props.updateGeneralInfo( payload )
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){
      
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }

  handleChange( event ) {
    const target = event.target
    const value = target.value
    const name = target.name

    if ( ( this.state.male || this.state.female ) && this.state.firstName && this.state.lastName ) {
      this.setState( {
        statesFilled: true,
      } )
    }

    this.setState( {
      [ name ]: value,
    } )
  }
  functionClicked() {
    this.setState( {
      closeClicked: true,
    } )
  }
  // updateGender( type, e ) {
  //   e.preventDefault()
  //   this.setState( { gender: type, femaleChecked: type == gender.Female && e.target.checked, maleChecked: type == gender.Male && e.target.checked } )
  // }

  render() {
    return (
      <div className="GeneralInfo">
        <BreadcrumbsSettings { ...this.state } />
        {this.props.message === true && (
          <div className="GeneralInfo__Success" style={ { display: this.state.closeClicked ? 'none' : 'flex' } }>
            <span className="GeneralInfo__Success--text">Success!</span>
            <span className="GeneralInfo__Success--text" style={ { fontWeight: '400' } }> All changes have been changed.</span>
            <button className="GeneralInfo__Success--button" onClick={ this.functionClicked }>
              <img src={ closeNotification } style={ { width: 13 } }/>
            </button>
          </div>
        )}
        {this.props.message !== true && this.props.message !== null  && (
          <div className="GeneralInfo__Failure" style={ { display: this.state.closeClicked ? 'none' : 'flex' } }>
            <span className="GeneralInfo__Failure--text">Whooups!</span>
            <span className="GeneralInfo__Failure--text" style={ { fontWeight: '400' } }> Something went wrong!</span>
            <button className="GeneralInfo__Failure--button" onClick={ this.functionClicked }>
              <img src={ closeNotification } style={ { width: 13 } }/>
            </button>
          </div>
        )}

        <h1 className="GeneralInfo__heading">General Account Information</h1>
        <h3 className="GeneralInfo__title">Enter your account information</h3>
        <form onSubmit={ this.handleSubmit }>
          <input
            className="GeneralInfo__input"
            type="text"
            name="firstName"
            placeholder="First name"
            value={ this.state.firstName }
            onChange={ this.handleChange }
          />
          <input
            className="GeneralInfo__input"
            type="text"
            name="lastName"
            placeholder="Last name"
            value={ this.state.lastName }
            onChange={ this.handleChange }
          />
          <div className="GeneralInfo__gender">
            <span className="GeneralInfo__title">Gender</span>
            <input
              className="GeneralInfo__checkbox"
              name="male"
              type="checkbox"
              checked={ this.state.male }
              onChange={ this.handleCheckboxChangeMale }
            />
            <label> Male </label>
            <input
              className="GeneralInfo__checkbox"
              name="female"
              type="checkbox"
              checked={ this.state.female }
              onChange={ this.handleCheckboxChangeFemale }
            />
            <label> Female </label>
          </div>
          <input
            style={ { backgroundColor: this.state.statesFilled ? '#22c5d4' : '#d1d1d6' } }
            disabled={ !this.state.statesFilled }
            className="GeneralInfo__button"
            type="submit"
            name="submit"
            value="SAVE CHANGES"
          />
        </form>
      </div>
    )
  }
}

export default ( GeneralInfo )
