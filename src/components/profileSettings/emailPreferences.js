import React, { Component } from 'react'
import '../../scss/EmailPreferences.css'
import Switch from 'react-switch'
import { BreadcrumbsSettings } from '../../components/common'
import closeNotification from '../../photos/close-notification.png'

class EmailPreferences extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      uid: this.props.uid,
      email: '',
      title: 'Edit Email Preferences',
      changed: false,
      checkedNotifications: false,
      checkedNewsletters: false,
      checkedDietStatus: false,
      closeClicked: false,
    }

    this._handleChange = this.handleChange.bind( this )
    this._handleSubmit = this.handleSubmit.bind( this )
    this._handleChangeNotifications = this.handleChangeNotifications.bind( this )
    this._handleChangeNewsletters = this.handleChangeNewsletters.bind( this )
    this._handleChangeDietStatus = this.handleChangeDietStatus.bind( this )
    this.functionClicked = this.functionClicked.bind( this )
  }

  componentWillMount() {
    setTimeout( () => { this.props.loadEmailPreferences( this.props.uid ) }, 2000 )

  }

  componentWillReceiveProps( nextProps ) {
    if ( nextProps ) {
      this.setState( { 'uid': nextProps.uid } )
      if ( nextProps.preferences &&
         ( nextProps.preferences[ 2 ].checked !== this.state.checkedDietStatus ||
          nextProps.preferences[ 1 ].checked !== this.state.checkedNewsletters ||
          nextProps.preferences[ 0 ].checked !== this.state.checkedDietStatus ) ) {
        this.setState( {
          uid: nextProps.uid,
          checkedDietStatus: nextProps.preferences[ 2 ].checked,
          checkedNewsletters: nextProps.preferences[ 1 ].checked,
          checkedNotifications: nextProps.preferences[ 0 ].checked
        } )
      }
    }
  }

  handleChangeNotifications( checkedNotifications ) {
    this.setState( { checkedNotifications, changed: true } )

    if ( !checkedNotifications ) {
      this.setState( {
        checkedNewsletters: false,
        checkedDietStatus: false,
      } )
    }
    this.props.updatePreference( 'allowNotifications', checkedNotifications )
  }

  handleChangeNewsletters( checkedNewsletters ) {
    this.setState( { checkedNewsletters, changed: true } )
    this.props.updatePreference( 'newsletters', checkedNewsletters )
  }

  handleChangeDietStatus( checkedDietStatus ) {
    this.setState( { checkedDietStatus, changed: true } )
    this.props.updatePreference( 'dietChallengeStatus', checkedDietStatus )
  }

  handleSubmit( event ) {
    this.setState( {
      closeClicked: false,
    } )
    let payload = {
      email: this.props.email,
      allowNotifications: this.state.checkedNotifications,
      newsletters: this.state.checkedNewsletters,
      dietChallengeStatus: this.state.checkedDietStatus,
      uid: this.state.uid
    }

    this.props.updateEmailPreferences( payload )
    event.preventDefault()
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){
      
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }

  handleChange( event ) {
    let newValue = event.target.value
    this.setState( { email: newValue, changed: true } )
    this.props.updatePreference( 'email', newValue )
  }

  functionClicked() {
    this.setState( {
      closeClicked: true,
    } )
  }
  render() {
    return (
      <div className="EmailPreferences">
        <BreadcrumbsSettings { ...this.state } />
        {this.props.message === true && (
          <div className="EmailPreferences__Success" style={ { display: this.state.closeClicked ? 'none' : 'flex' } }>
            <span className="EmailPreferences__Success--text">Success!</span>
            <span className="EmailPreferences__Success--text" style={ { fontWeight: '400' } }> All changes have been changed.</span>
            <button className="EmailPreferences__Success--button" onClick={ this.functionClicked }>
              <img src={ closeNotification } style={ { width: 13 } }/>
            </button>
          </div>
        )}
        {this.props.message !== true && this.props.message !== null  && (
          <div className="EmailPreferences__Failure" style={ { display: this.state.closeClicked ? 'none' : 'flex' } }>
            <span className="EmailPreferences__Failure--text">Whooups!</span>
            <span className="EmailPreferences__Success--text" style={ { fontWeight: '400' } }> Invalid email adress.</span>
            <button className="EmailPreferences__Failure--button" onClick={ this.functionClicked }>
              <img src={ closeNotification } style={ { width: 13 } }/>
            </button>
          </div>
        )}
        <h1 className="EmailPreferences__heading">Email Preferences</h1>
        <h3 className="EmailPreferences__title">Enter your valid email address</h3>
        <form onSubmit={ this._handleSubmit }>
          <input
            className="EmailPreferences__input"
            type="text"
            name="email"
            placeholder="Email address"
            value={ this.props.email }
            onChange={ this._handleChange }
          />
          <div className="EmailPreferences__single">
            <p className="EmailPreferences__switch-header">Allow notifications</p>

            <Switch
              className="EmailPreferences__switch"
              onChange={ this._handleChangeNotifications }
              checked={ this.state.checkedNotifications }
              onColor="#cfeef1"
              offColor="#c4c4c9"
              onHandleColor="#22c5d4"
              offHandleColor="#fff"
              handleDiameter={ 20 }
              uncheckedIcon={ false }
              checkedIcon={ false }
              boxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
              activeBoxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
              height={ 12 }
              width={ 30 }
              id="switchNotifications"
            />
            <p className="EmailPreferences__switch-exp">
            You wil receive email about <br />
            Diet Challenge status and newsletters
            </p>
          </div>
          <div className="EmailPreferences__single">
            <p className="EmailPreferences__switch-header">Newsletters</p>
            <Switch
              className="EmailPreferences__switch"
              checked={ this.state.checkedNewsletters }
              disabled={ !this.state.checkedNotifications }
              onChange={ this._handleChangeNewsletters }
              onColor="#cfeef1"
              offColor="#c4c4c9"
              onHandleColor="#22c5d4"
              offHandleColor="#fff"
              handleDiameter={ 20 }
              uncheckedIcon={ false }
              checkedIcon={ false }
              boxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
              activeBoxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
              height={ 12 }
              width={ 30 }
              id="switchNewsletters"
            />
          </div>
          <div className="EmailPreferences__single">
            <p className="EmailPreferences__switch-header">Diet challenge status</p>
            <Switch
              className="EmailPreferences__switch"
              checked={ this.state.checkedDietStatus }
              disabled={ !this.state.checkedNotifications }
              onChange={ this._handleChangeDietStatus }
              onColor="#cfeef1"
              offColor="#c4c4c9"
              onHandleColor="#22c5d4"
              offHandleColor="#fff"
              handleDiameter={ 20 }
              uncheckedIcon={ false }
              checkedIcon={ false }
              boxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
              activeBoxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
              height={ 12 }
              width={ 30 }
              id="switchDietStatus"
            />
          </div>
          <input
            style={ { backgroundColor: this.state.changed ? '#22c5d4' : '#d1d1d6' } }
            disabled={ !this.state.email && !this.state.changed }
            className="EmailPreferences__button"
            type="submit"
            name="submit"
            value="SAVE CHANGES"
            onClick={ this._handleSubmit }
          />
        </form>
      </div>
    )
  }
}

export default ( EmailPreferences )
