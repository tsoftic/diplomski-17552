import React, { Component } from 'react'
import { BreadcrumbsSettings } from '../../components/common'
import '../../scss/AccountSecurity.css'
import closeNotification from '../../photos/close-notification.png'

class AccountSecurity extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      uid: '',
      title: 'Edit Account Security',
      currentPassword: '',
      newPassword: '',
      confirmNewPassword: '',
      statesFilled: false,
      clicked: false,
      closeClicked: false,

    }

    this.handleChange = this.handleChange.bind( this )
    this.handleSubmit = this.handleSubmit.bind( this )
    this.redirectToLogin = this.redirectToLogin.bind( this )
    this.functionClicked = this.functionClicked.bind( this )
  }

  handleChange( event ) {
    event.preventDefault()
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState( {
      [ name ]: value,
    } )

    this.state.statesFilled = ( this.state.currentPassword
                             && this.state.newPassword
                             && this.state.confirmNewPassword )
  }
  redirectToLogin() {
    return this
      .props
      .loginProps
      .history
      .push( "/login" )
  }

  handleSubmit( event ) {
    this.setState( {
      closeClicked: false,
    } )
    let payload = {
      uid: this.props.uid,
      currentPassword: this.state.currentPassword,
      newPassword: this.state.newPassword,
      confirmNewPassword: this.state.confirmNewPassword
    }

    this.props.changePassword( payload )
    event.preventDefault()
  }

  functionClicked() {
    this.setState( {
      closeClicked: true,
    } )
  }
  render() {
    return (

      <div className="AccountSecurity">
        <BreadcrumbsSettings { ...this.state } />
        {this.props.message === true && (
          <div className="AccountSecurity__Success" style={ { display: this.state.closeClicked ? 'none' : 'flex' } }>
            <span className="AccountSecurity__Success--text">Success!</span>
            <span className="AccountSecurity__Success--text" style={ { fontWeight: '400' } }> Password changed.</span>
            <button className="AccountSecurity__Success--button" onClick={ this.functionClicked }>
              <img src={ closeNotification } style={ { width: 13 } }/>
            </button>
          </div>
        )}
        {this.props.message !== true && this.props.message !== null  && (
          <div className="AccountSecurity__Failure" style={ { display: this.state.closeClicked ? 'none' : 'flex' } }>
            <span className="AccountSecurity__Failure--text">Whooups!</span>
            <span className="AccountSecurity__Success--text" style={ { fontWeight: '400' } }> Password do not match.</span>
            <button className="AccountSecurity__Failure--button" onClick={ this.functionClicked }>
              <img src={ closeNotification } style={ { width: 13 } }/>
            </button>
          </div>
        )}
        <h1 className="AccountSecurity__heading">Account Security</h1>
        <h3 className="AccountSecurity__title">Manage your account password</h3>
        <form onSubmit={ this.handleSubmit }>
          <input
            className="AccountSecurity__input"
            type="password"
            name="currentPassword"
            placeholder="Current Password"
            value={ this.state.currentPassword }
            onChange={ this.handleChange }
          />
          <span className="AccountSecurity__subtitle"> Update new password</span>
          <span className="AccountSecurity__text"> Re-enter your new password in order to update new password </span>
          <input
            className="AccountSecurity__input"
            type="password"
            name="newPassword"
            placeholder="New Password"
            value={ this.state.newPassword }
            onChange={ this.handleChange }
          />
          <input
            className="AccountSecurity__input"
            type="password"
            name="confirmNewPassword"
            placeholder="Re-enter New Password"
            value={ this.state.confirmNewPassword }
            onChange={ this.handleChange }
          />
          <input
            style={ { backgroundColor: this.state.statesFilled ? '#22c5d4' : '#d1d1d6' } }
            disabled={ !this.state.statesFilled }
            className="AccountSecurity__button"
            type="submit"
            name="submit"
            value="SAVE CHANGES"
          />
        </form>
      </div>
    )
  }
}

export default ( AccountSecurity )
