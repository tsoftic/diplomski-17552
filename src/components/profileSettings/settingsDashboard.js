import React, { Component } from 'react'
import '../../scss/SettingsDashboard.css'

import unisexAvatar from '../../photos/avatar-unisex.png'



class SettingsDashboard extends Component {
  
logOut = () => {
  window.localStorage.removeItem('carola');
  this
  .props
  .history
  .push( "/login" )

}

render() {

  const user = JSON.parse(localStorage.getItem('carola'));

  return (
    <div className="SettingsDashboard">
      <img className="SettingsDashboard__profilePic" src={unisexAvatar} alt="Profile picture"/>
      <h3 className="SettingsDashboard__name"> { user ? user.displayName : 'John Doe'}</h3>
      <h1 className="SettingsDashboard__heading">Account Settings</h1>
      <div className="SettingsDashboard__boxes">
        <a href="/general" className="SettingsDashboard__boxes--single grey">
          <span className="SettingsDashboard__boxes--title">General information</span>
          <span className="SettingsDashboard__boxes--subtitle">Basic account information</span>

        </a>
        <a href="/emailPreferences" className="SettingsDashboard__boxes--single grey">
          <span className="SettingsDashboard__boxes--title">Email</span>
          <span className="SettingsDashboard__boxes--subtitle">Email preferences</span>

        </a>
        <a href="/security" className="SettingsDashboard__boxes--single grey">
          <span className="SettingsDashboard__boxes--title">Security</span>
          <span className="SettingsDashboard__boxes--subtitle">Update your password</span>
        </a>
      </div>
      <a href="#" onClick={this.logOut} className="SettingsDashboard__logout">Log Out</a>
    </div>
  )
}

}

export default SettingsDashboard
