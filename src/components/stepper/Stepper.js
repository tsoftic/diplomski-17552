import React, { Component } from 'react'
import _ from 'lodash'
import './Stepper.css'
class Stepper extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      currentStep: 0,
      answers: [],
      lastStep: false,
    }

    this.renderNumbers = this.renderNumbers.bind( this )
    this.renderTopSeparator = this.renderTopSeparator.bind( this )
    this.renderBotSeparator = this.renderBotSeparator.bind( this )
    this.renderStepperContent = this.renderStepperContent.bind( this )
    this.renderLastStep = this.renderLastStep.bind( this )
    this.postAnswer = this.postAnswer.bind( this )
  }

  renderTopSeparator( index ) {
    if ( index === 0 ) return null
    let done = index <= this.state.currentStep
    let separatorClassName = done ? 'Step-number-separator done' : 'Step-number-separator'

    return <div className={ separatorClassName }></div>
  }

  renderBotSeparator( index, hidden ) {
    if ( hidden ) return null
    let done = index <= this.state.currentStep
    let separatorClassName = done ? 'Step-number-separator done' : 'Step-number-separator'

    return <div className={ separatorClassName }></div>
  }

  renderNumbers() {
    const { data } = this.props

    return data.map( ( item, index ) => {
      let innerClassName = 'Step-number-circle-inner'
      let circleClassName = 'Step-number-circle'
      const separatorHidden = index === data.length - 1

      if ( index <= this.state.currentStep ) {
        innerClassName += ' done'
        circleClassName += ' done'
      }

      return (
        <div
          key={ index }
          className="Step-number"
        >
          { this.renderTopSeparator( index, separatorHidden ) }
          <div className={ circleClassName }>
            <div className={ innerClassName }>
              { index + 1 }
            </div>
          </div>
          { this.renderBotSeparator( index, separatorHidden ) }
        </div>
      ) }
    )
  }

  renderStepperContent() {
    const { data } = this.props
    const currentStep = data[ this.state.currentStep ]
    return (
      <div className="Step-container">
        <div className="Step-container-content">
          <h1>{ currentStep.title }</h1>
          {currentStep.extraData}

          <div className="Step-container-content-buttons">
            <div
              className="Step-button no"
              onClick={ () => this.postAnswer( true ) }
            >
              {currentStep.continue === 'false' && 'YES'}
              {currentStep.continue === 'true' && 'CONTINUE'}
            </div>

            {
              currentStep.continue === 'false' && (
                <div
                  className="Step-button no"
                  onClick={ () => this.postAnswer( false ) }
                >
                  NO
                </div>
              )
            }

            {
              ( currentStep.key !== 1 && currentStep.key !== 5 ) && (
                <div
                  className="Step-button goBack"
                  onClick={ () => this.goBack( currentStep.key ) }
                >
                  GO BACK
                </div>
              )
            }

            
          </div>
        </div>
        <div className="Step-container-image">
          <img src={ currentStep.image } alt={ currentStep.title } />
        </div>
      </div>
    )
  }

  renderLastStep() {
    this.props.history.push( '/mealPlan' )
  }

  goBack( currentStep ) {
    this.setState( {
      currentStep: this.state.currentStep - 1,
    } )
  }

  postAnswer( answer ) {
    const answers = this.state.answers
    answers.push( { index: this.state.currentStep, value: answer } )

    
    if ( this.state.currentStep + 1 === this.props.data.length ) {
      this.setState( { lastStep: true } )
      return this.props.onStepperEnd( this.state.answers )
    } else if ( this.state.currentStep === 0 && !answers[ 0 ].value ) {
      answers.push( { index: 1, value: false } )
      this.setState( {
        currentStep: 2,
        answers,
      } )
    } else {
      this.setState( {
        currentStep: this.state.currentStep + 1,
        answers,
      } )
    }
  }

  render() {
    return (
      <div className="Step">
        <div className="Step-numbers-container">
          { this.renderNumbers() }
        </div>
        { this.state.lastStep ? this.renderLastStep() : this.renderStepperContent() }
      </div>
    )
  }
}

export default Stepper
