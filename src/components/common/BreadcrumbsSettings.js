import React,{ Component } from 'react'
import Arrow from '../../photos/arrow.png'
import '../../scss/BreadCrumbsSettings.css'

class BreadcrumbsSettings extends Component {
  render() {
    return(
      <div className="BreadcrumbsSettings">
        <div className="BreadcrumbsSettings__container">
          <a href="/settings" className="cursor"><span className="BreadcrumbsSettings__acc">Account Settings</span></a>
          <img className="BreadcrumbsSettings__img"src={ Arrow } alt="arrow"/>
          <span className="BreadcrumbsSettings__thisPage" >{this.props.title}</span>
        </div>
      </div>
    )
  }
}

export { BreadcrumbsSettings }
