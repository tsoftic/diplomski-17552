import React,{ Component } from 'react'
import Arrow from '../../photos/arrow.png'
import '../../scss/BreadcrumbsMeal.css'

class BreadcrumbsMeal extends Component {
  redirect() {
  }
  render() {
    return(
      <div className="BreadcrumbsSettings">
        <div className="BreadcrumbsSettings__container">
          <a href="/mealPlan" className="cursor"><span className="BreadcrumbsSettings__acc">Carola Diet</span></a>
          <img className="BreadcrumbsSettings__img"src={ Arrow } alt="arrow"/>
          { this.props.challengeWeek !==undefined &&
            <span className="BreadcrumbsSettings__thisPage" >{this.props.challengeWeek.title}</span>
          }
          { this.props.challengeWeek ===undefined &&
            <span onClick ={ () => this.redirect()}className="BreadcrumbsSettings__acc cursor" >{this.props.title}</span>
          }
          { this.props.challengeWeek ===undefined &&
            <img className="BreadcrumbsSettings__img"src={ Arrow } alt="arrow"/>
          }
          { this.props.challengeWeek ===undefined &&
            <span className="BreadcrumbsSettings__thisPage" >{this.props.mealProps.title}</span>
          }
        </div>
      </div>
    )
  }
}

export { BreadcrumbsMeal }
