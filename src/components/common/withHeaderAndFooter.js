import React, { Component } from 'react'
import hoistStatics from 'hoist-non-react-statics'
import Header from './Header'
import Footer from './Footer'

const withHeaderAndFooter = () => BaseComponent => {

  class WithHeaderAndFooter extends Component {

    render() {
      return (
        <div className="Main">
          <Header {...this.props.location}/>
          <BaseComponent { ...this.props } />
          <Footer />
        </div>
      )
    }
  }

  return hoistStatics( WithHeaderAndFooter, BaseComponent )
}

export { withHeaderAndFooter }
