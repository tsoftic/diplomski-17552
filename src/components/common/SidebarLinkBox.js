import React from 'react'
import '../../scss/SidebarLinkBox.css'

const SidebarLinkBox = ( { greyLinks, title, links } ) => (
  <div className="SidebarLinkBox">
    <h3>{ title }</h3>
    <ul>
      {
        links.map( link => (
          <li className="SidebarLinkBox__link">
            <a href={ link.route } className={ greyLinks ? 'grey' : 'blue' }>{ link.title }</a>
          </li>
        ) )
      }
    </ul>
  </div>
)

export { SidebarLinkBox }
