import React, { Component } from 'react'
import '../../scss/HeaderNew.css'
import '../../scss/Common.css'
import logo from '../../photos/carolaLogo.png'
import diet from '../../photos/svg/diet.svg'
import home from '../../photos/svg/home.svg'
import profile from '../../photos/svg/profile.svg'
import diet_full from '../../photos/svg/diet_full.svg'
import home_full from '../../photos/svg/home_full.svg'
import profile_full from '../../photos/svg/profile_full.svg'

class HeaderNew extends Component {
  constructor( props ) {
    super( props )

    this.state = {
      dashboard:false,
      mealPlan:false,
      profile:false
    }
  }
  componentWillMount() {
    this.fillIcon(this.props.pathname)
  }
  fillIcon(path){
    if(path==="/dashboard" || path==="/triggers"|| path==="/symptoms"){
      this.setState({
        dashboard:true,
        mealPlan:false,
        profile:false
      })
    }
    if(path==="/mealPlan" || path.substring(0,9)==="/dayMeals" ||path.substring(0,5)==="/meal"){
      this.setState({
        dashboard:false,
        mealPlan:true,
        profile:false
      })
    }
    if( path==="/settings"||path==="/general"||path==="/emailPreferences"||path==="/security" ){
      this.setState({
        dashboard:false,
        mealPlan:false,
        profile:true
      })
    }
  }
  render() {
    return (
      <div className="Header font">
        <div className="Header__container">
          <div className=" Header__logo picture">
            <img style={ { maxWidth: 70, maxHeight: 42} } src={ logo }/>
          </div>
          <div className="Header__container--nav">
            <ul>
              <li id="dashboard" className="dashboard">
                <a href="/dashboard">
                  {!this.state.dashboard && <img src={ home } alt="Dashboard"/>}
                  {this.state.dashboard && <img src={ home_full } alt="Dashboard"/>}
                  <label>Dashboard</label>
                </a>
              </li>
              <li id="mealPlan">
                <a href="/mealPlan">
                  {!this.state.mealPlan && <img src={ diet } alt="Meal plan"/>}
                  {this.state.mealPlan && <img src={ diet_full } alt="Meal plan"/>}
                  <label>Carola diet</label>
                </a>
              </li>
            </ul>
          </div>
          <div className="Header__container--profile">
            <a href="/settings">
              {!this.state.profile && <img  className="Header__container--userPicture" src={ profile } alt="image"/>}
              {this.state.profile && <img  className="Header__container--userPicture" src={ profile_full } alt="image"/>}
              <label> Profile </label>
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default HeaderNew
