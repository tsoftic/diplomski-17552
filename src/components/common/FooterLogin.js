import React, { Component } from 'react'
import '../../scss/Footer.css'
import '../../scss/Common.css'

class Footer extends Component {

  componentDidMount() {

  }

  render() {
    return (
      <div className="main-footer">
        <ul >
          <li
          style={ {
                    textDecoration: 'none',
                    fontSize: '16px',
                    color: 'rgb(142, 142, 147)'
                  } }>
            ABOUT CAROLA
          </li>
          <li 
          style={ {
            textDecoration: 'none',
            fontSize: '16px',
            color: 'rgb(142, 142, 147)'
          } }>
            PRIVACY POLICY
          </li>
          <li
          style={ {
            textDecoration: 'none',
            fontSize: '16px',
            color: 'rgb(142, 142, 147)'
          } }>
            NEED HELP?
          </li>
        </ul>
      </div>
    )
  }
}

export default Footer
