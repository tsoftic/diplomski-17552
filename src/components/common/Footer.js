import React, { Component } from 'react'
import { a } from 'react-router-dom'
import '../../scss/FooterNew.css'
import '../../scss/Common.css'

class FooterNew extends Component {

  componentDidMount() {

  }

  render() {
    return (
      <div className="footer">
        <div className="wrapper">
          <div className="navFooter">
            <ul >
              <li  style={ {
                    textDecoration: 'none',
                    fontSize: '16px',
                    color: 'rgb(142, 142, 147)'
                  } }>
                ABOUT 
              </li>
              <li  style={ {
                    textDecoration: 'none',
                    fontSize: '16px',
                    color: 'rgb(142, 142, 147)'
                  } }>
                PRIVACY POLICY
              </li>
              <li  style={ {
                    textDecoration: 'none',
                    fontSize: '16px',
                    color: 'rgb(142, 142, 147)'
                  } }>
                NEED HELP?
              </li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default FooterNew
