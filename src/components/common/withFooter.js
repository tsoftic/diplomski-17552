import React, { Component } from 'react'
import hoistStatics from 'hoist-non-react-statics'
import Footer from './Footer'
import '../../scss/App.css'


const withFooter = () => BaseComponent => {

  class WithFooter extends Component {

    render() {
      return (
        <div className="Main">
        <div  className="withFooter">
          <BaseComponent { ...this.props } />
          </div>
          <Footer/>
        </div>
      )
    }
  }

  return hoistStatics( WithFooter, BaseComponent )
}

export { withFooter }
