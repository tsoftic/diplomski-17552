import React, { Component } from 'react'
import '../../scss/AddMealBox.css'
import { symptom } from "./../../utilities/enum"
class AddMealBox extends Component {

  constructor( props ) {
    super( props )
    this.state = {
      mealOutSideDiet: ''
    }
    this.onSubmit = this.onSubmit.bind( this )
    this.handleChange = this.handleChange.bind( this )
  }

  onSubmit () {
    this.props.parentObject.updateSymptoms( this.state.mealOutSideDiet, symptom.MealOutsideDiet )
  }

  handleChange( event ) {
    event.preventDefault()
    const target = event.target
    const value = target.value

    this.setState( {
      mealOutSideDiet: value
    } )
  }
  render() {
    return (
      <div className="AddMealBox">
        <h2>{this.props.title}</h2>
        <input type="text" placeholder={ this.props.placeholder } className="AddMealBox__input"  onChange={ ( e ) => this.handleChange( e ) } />
        <input type="button" className="AddMealBox__button" value="Add this meal" onClick={ this.onSubmit }/>
      </div>
    )
  }
}

export { AddMealBox }
