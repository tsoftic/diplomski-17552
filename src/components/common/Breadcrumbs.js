import React,{ Component } from 'react'
import Arrow from '../../photos/arrow.png'
import '../../scss/Breadcrumbs.css'

class Breadcrumbs extends Component {
  render() {
    return(
      <div className="Breadcrumbs">
        <div className="Breadcrumbs__container">
          <a href="/dashboard" className="cursor"><span className="Breadcrumbs__acc">Dashboard</span></a>
          <img className="Breadcrumbs__img"src={ Arrow } alt="arrow"/>
          <span className="Breadcrumbs__thisPage" >{this.props.head}</span>
        </div>
      </div>
    )
  }
}

export { Breadcrumbs }
