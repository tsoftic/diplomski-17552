import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import '../../scss/SymptomSlider.css'

const SliderNotch = ( { small } ) => (
  <div className={ `SliderNotch ${ small ? 'small' : null }` }>
    <div className={ `SliderNotch__notch` }></div>
  </div>
)


class SliderBar extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      relX: 0,
      currentStep: 1,
      initialStep: 1,
      snapOffset: 25,
    }

    this.SNAP_OFFSET = window.innerWidth > 600 ? 25 : 15

    if ( this.props.small ) {
      this.SNAP_OFFSET = 15
    }

    this._onMouseMove = this.onMouseMove.bind( this )
    this._onMouseUp = this.onMouseUp.bind( this )
    this._onTouchMove = this.onTouchMove.bind( this )
  }

  _onMouseDown( e ) {
    if ( e.button !== 0 ) return
    document.addEventListener( 'mousemove', this._onMouseMove )
    document.addEventListener( 'touchmove', this._onMouseMove )
    document.addEventListener( 'mouseup', this._onMouseUp )
    document.addEventListener( 'touchend', this._onMouseUp )
    // var pos = $( this.getDOMNode() ).offset()
    const ref = ReactDOM.findDOMNode( this.refs.handle )
    const body = document.body
    const box = ref.getBoundingClientRect()

    this.setState( {
      relX: e.pageX - ( box.left + body.scrollLeft - body.clientLeft ),
    } )
    e.stopPropagation()
    e.preventDefault()
  }

  onMouseMove( e ) {
    const ref = ReactDOM.findDOMNode( this.refs.handle )
    const body = document.body
    const box = ref.getBoundingClientRect()

    const moved = ( e.pageX - ( box.left + body.scrollLeft - body.clientLeft ) ) - this.state.relX

    let snap = 0
    let snapOffset = 0

    if ( moved >= 0 ) {
      snap = ( moved + this.SNAP_OFFSET ) / ( this.SNAP_OFFSET * 2 )
      snapOffset = parseInt( snap,10 ) + this.state.initialStep
    } else {
      snap = ( moved - this.SNAP_OFFSET ) / ( this.SNAP_OFFSET * 2 )
      snapOffset = parseInt( snap,10 ) + this.state.initialStep
    }

    if ( snapOffset >= 0 && snapOffset <= 10 ) {
      this.setState( { currentStep: snapOffset } )
    } else if ( snap > 10 ) {
      this.setState( { currentStep: 10 } )
    } else {
      this.setState( { currentStep: 10 } )
    }
    e.preventDefault()
  }

  onTouchMove( e ) {
    const ref = ReactDOM.findDOMNode( this.refs.handle )
    const body = document.body
    const box = ref.getBoundingClientRect()

    const moved = ( e.changedTouches[ 0 ].pageX - ( box.left + body.scrollLeft - body.clientLeft ) ) - this.state.relX

    let snap = 0
    let snapOffset = 0

    if ( moved >= 0 ) {
      snap = ( moved + this.SNAP_OFFSET ) / ( this.SNAP_OFFSET * 2 )
      snapOffset = parseInt( snap,10 ) + this.state.initialStep
    } else {
      snap = ( moved - this.SNAP_OFFSET ) / ( this.SNAP_OFFSET * 2 )
      snapOffset = parseInt( snap,10 ) + this.state.initialStep
    }

    if ( snapOffset >= 0 && snapOffset <= 10 ) {
      this.setState( { currentStep: snapOffset } )
    } else if ( snap > 10 ) {
      this.setState( { currentStep: 10 } )
    } else {
      this.setState( { currentStep: 10 } )
    }
  }

  onMouseUp( e ) {
    this.props.update( this.state.currentStep )
    this.setState( { initialStep: this.state.currentStep } )
    document.removeEventListener( 'mousemove', this._onMouseMove )
    document.removeEventListener( 'touchmove', this._onMouseMove )
    document.removeEventListener( 'mouseup', this._onMouseUp )
    document.removeEventListener( 'touchend', this._onMouseUp )
    e.preventDefault()
  }

  render() {
    const offset = this.state.currentStep * 10
    const calculatedOffset = `calc( ${ offset }% - ${ this.SNAP_OFFSET }px )`

    return (
      <div className="SliderBar" ref="handle">
        <div
          className="SliderBar__handle"
          onTouchStart={ ( { nativeEvent } ) => this._onMouseDown( nativeEvent ) }
          onTouchMove={ ( { nativeEvent } ) => this._onTouchMove( nativeEvent ) }
          onMouseDown={ this._onMouseDown.bind( this ) }
          style={ { left: calculatedOffset } }
        >
        </div>
        <div className="SliderBar__fill" style={ { width: calculatedOffset } }></div>
      </div>
    )
  }
}

class SymptomSlider extends Component {

  renderNotches = () => {
    const steps = 10
    let render = []

    for ( let i = 0; i < steps; i++ ) {
      render.push( <SliderNotch small={ this.props.small === true } key={ i } /> )
    }

    return render
  }

  render() {
    let mainClass = 'SymptomSlider'
    if(this.props.color === "red") mainClass= 'SymptomSlider red'
    else if(this.props.color === "blue") mainClass='SymptomSlider blue'
       
    const leftText = this.props.leftText ? this.props.leftText : 'None'
    const rightText = this.props.rightText ? this.props.rightText : 'Severe'

    return (
      <div className={ mainClass }>
        <h2>{ this.props.title }</h2>
        <div className="SymptomSlider__container">
          <div className="SymptomSlider__container--slider">
            <div className="SymptomSlider__container--slider-notches">
              { this.renderNotches() }
            </div>
            <SliderBar small={ this.props.small } update={ this.props.onSliderUpdate } />
            <div className="SymptomSlider__container--slider-notches">
              { this.renderNotches() }
            </div>
          </div>
          <div className="SymptomSlider__container--legend">
            <span>{ leftText }</span>
            <span>{ rightText }</span>
          </div>
        </div>
      </div>
    )
  }
}

export { SymptomSlider }
