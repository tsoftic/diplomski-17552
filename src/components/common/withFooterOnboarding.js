import React, { Component } from 'react'
import hoistStatics from 'hoist-non-react-statics'
import Footer from './Footer'
import '../../scss/App.css'


const withFooterOnboarding = () => BaseComponent => {

  class withFooterOnboarding extends Component {

    render() {
      return (
        <div className="Main">
        <div>
          <BaseComponent { ...this.props } />
          </div>
          <Footer/>
        </div>
      )
    }
  }

  return hoistStatics( withFooterOnboarding, BaseComponent )
}

export { withFooterOnboarding }
