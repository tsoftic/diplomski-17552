import React,{Component} from 'react'
import '../../scss/OnboardingProfile.css'

import unisexAvatar from '../../photos/avatar-unisex-white.png'

class OnboardingProfile extends Component {
  constructor( props ) {
    super( props )
  }
  componentDidMount(){
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){
      
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }
  render(){
    const user = JSON.parse( localStorage.getItem( 'carola' ) )
    return (
      <div >
        <div className="backgroundFood"></div>
        <div className="onboardingProfile">
          <img className="onboardingProfile__profilePic" src={ unisexAvatar } alt="Profile picture"/>
          <h3 className="onboardingProfile__intro">Welcome, { user ? user.displayName : 'John Doe'}</h3>
          <h5 className="onboardingProfile__text">We&rsquo;re excited to provide you innovative tools to help you feel your best and stay on top of your diet.</h5>
          <h5 className="onboardingProfile__letsstart">
          Let&rsquo;s start by finding out little bit more information...
          </h5>
          <a href="/diagnosis" className="onboardingProfile__turquoisebutton">
            <span>GET STARTED</span>
          </a>
        </div>
      </div>
    )
  }   
}

export default OnboardingProfile
