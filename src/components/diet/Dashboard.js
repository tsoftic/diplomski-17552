import React,{Component} from 'react'
import '../../scss/Dashboard.css'
const imgTriggers = require( '../../assets/triggers.png' )
const imgSymptoms = require( '../../assets/symptoms.png' )
class Diet extends Component {
  constructor( props ) {
    super( props )
  }

  render(){
    const user = JSON.parse( localStorage.getItem( 'carola' ) )
    return (
      <div className="Diet">
        <h1 className="Diet__heading">Dashboard</h1>
        <h3 className="Diet__intro">Log your symptoms as often as you can</h3>
        <div className="Diet__boxes">
          <a href="/symptoms" className="Diet__boxes--single blue">
            <img src={ imgSymptoms } alt=""/>
            <span>Symptoms</span>
          </a>
          <a href="/triggers" className="Diet__boxes--single red">
            <img src={ imgTriggers } alt=""/>
            <span>Triggers</span>
          </a>
        </div>
      </div>
    )
  }
}

export default Diet
