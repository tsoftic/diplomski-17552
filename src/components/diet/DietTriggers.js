import React, { Component } from 'react'
import '../../scss/DietTriggers.css'
import Switch from 'react-switch'
import { Breadcrumbs } from '../../components/common'
import { triggerFood } from '../../utilities/enum'
import { getDate } from "../../utilities/date"

class TriggerRow extends Component {
  render() {
    return (
      <div className="TriggerRow">
        <div className="TriggerRow__content">
          <h4>{this.props.title}</h4>
          <span>{this.props.subtitle}</span>
        </div>
        <div className="TriggerRow__switch">
          <Switch
            className="switch"
            disabled = {true}
            checked={ this.props.checked }
            onChange= {()=>{}}
            onColor="#cfeef1"
            offColor="#c4c4c9"
            onHandleColor="#22c5d4"
            handleDiameter={ 20 }
            uncheckedIcon={ false }
            checkedIcon={ false }
            offHandleColor="#fff"
            boxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
            activeBoxShadow="0px 1px 4px 0px rgba(49, 49, 51, 0.32),0px 4px 16px 0px rgba(49, 49, 51, 0.2)"
            height={ 12 }
            width={ 30 }
            id="switch1"
          />
        </div>
      </div>
    )
  }
}

class DietTriggers extends Component {
  constructor( props ) {
    super( props )

    this.state = {
      head: 'Triggers',
      uid: props.uid,
      date: getDate().toString()
    }
  }

  componentWillMount() {
    setTimeout( () => { this.props.loadTrigger( this.props.uid ) }, 1000 )
  }

  componentWillReceiveProps() {
    this.setState( { uid: this.props.uid } )
  }
  update( title, value ) {
    switch ( title ) {
      case triggerFood.Pasta:
        this.props.updateTrigger( 'pasta', value )
        break
      case triggerFood.Meat:
        this.props.updateTrigger( 'meat', value )
        break
      case triggerFood.Seafood:
        this.props.updateTrigger( 'seafood', value )
        break
      case triggerFood.DiaryProducts:
        this.props.updateTrigger( 'diaryProducts', value )
        break
      case triggerFood.Vegetables:
        this.props.updateTrigger( 'vegetables', value )
        break
      case triggerFood.Fruits:
        this.props.updateTrigger( 'fruits', value )
        break
      default:
        console.warn( 'invalid trigger type' )
        break
    }

  }
  render() {
    return (
      <div className="DietTriggers">
        <Breadcrumbs { ...this.state } />
        <div className="DashboardGrid">
          <div className="DashboardGrid__left">
            <div className="DietTriggers__container">
              <h1 className="DashboardGrid__heading">Triggers</h1>
              <h2 className="DashboardGrid__subheadingText">
                Once you've completed the Diet, the switches below will indicate which
                groups are your trigger foods.
              </h2>
             
              {
                this.props.triggers.map( item => <TriggerRow title={ item.title } parentObject={ this } subtitle={ item.subtitle } checked={ item.checked } /> )
              }
            </div>
          </div>
        </div>

      </div>
    )
  }
}
export default DietTriggers
