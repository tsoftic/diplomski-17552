import React, { Component } from 'react'
import '../../scss/DietTriggers.css'
import '../../scss/Symptoms.css'
import '../../scss/DashboardGrid.css'
import { getDate } from '../../utilities/date.js'
// import Switch from 'react-switch'

import { Breadcrumbs, SymptomSlider, AddMealBox } from '../../components/common'
import { symptom } from '../../utilities/enum'

class DietTriggers extends Component {
  constructor( props ) {
    super( props )

    this.state = {
      head: 'Symptoms',
      overallCondition: 1,
      abdominalPain: 1,
      bloating: 1,
      gas: 1,
      lethargy: 1,
      nausea: 1,
      diarrhea: 1,
      constipation: 1,
      stressLevel: 1,
      uid: this.props.uid,
      date: getDate().toString(),
      mealOutSideDiet: ''
    }
  }

  componentWillMount() {
    this.setState( { uid: this.props.uid } )
  }

  updateSymptoms( value, type ) {
    switch ( type ) {
      case symptom.OverallCondition:
        this.setState( {
          overallCondition: value
        })
        break
      case symptom.AbdominalPain:
        this.setState( {
          abdominalPain: value
        })
        break
      case symptom.Bloating:
        this.setState( {
          Bloating: value
        })
        break
      case symptom.Gas:
        this.setState( {
          gas: value
        })
        break
      case symptom.Lethargy:
        this.setState( {
          lethargy: value
        })
        break
      case symptom.Nausea:
        this.setState( {
          nausea: value
        })
        break
      case symptom.Diarrhea:
        this.setState( {
          diarrhea: value
        })
        break
      case symptom.Constipation:
        this.setState( {
          constipation: value
        })
        break
      case symptom.StressLevel:
        this.setState( {
          stressLevel: value
        })
        break
      case symptom.MealOutsideDiet:
        this.setState( {
          mealOutSideDiet: value
        })
        break
      default:
        console.warn( 'checkout symptom types' )
        break
    }
  }

  update() {
    let payload = {
      overallCondition: this.state.overallCondition,
      abdominalPain: this.state.abdominalPain,
      abdominalBloating: this.state.bloating,
      gas: this.state.gas,
      lethargy: this.state.lethargy,
      nausea: this.state.nausea,
      diarrhea: this.state.diarrhea,
      constipation: this.state.constipation,
      stressLevel: this.state.stressLevel,
      uid: this.props.uid,
      date: getDate(),
      mealOutSideDiet: this.state.mealOutSideDiet
    }
    this.props.addDefaultSymptoms( payload )
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){
      this.props.history.push('/dashboard')
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }
  
  render() {
    return (
      <div className="Symptoms">
        <div className="DashboardGrid">
          <div className="DashboardGrid__left">
            <div className="Symptoms__container">
              <Breadcrumbs { ...this.state } />
              <h1 className="DashboardGrid__heading">Symptoms</h1>
              <h2 className="DashboardGrid__subheading">Rate your condition</h2>

              <SymptomSlider
                onSliderUpdate={ value => this.updateSymptoms( value, symptom.OverallCondition ) }
                title="Overall condition"
                color="blue"
                leftText="No symptoms"
                rightText="Severe symptoms"
              />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.Diarrhea ) } title="Diarrhea" />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.Constipation ) } title="Constipation" />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.AbdominalPain ) } title="Abdominal pain / Discomfort" />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.Bloating ) } title="Bloating" />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.Gas ) } title="Gas" />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.Lethargy ) } title="Lethargy" />
              <SymptomSlider onSliderUpdate={ value => this.updateSymptoms( value, symptom.Nausea ) } title="Nausea" />
              <SymptomSlider
                onSliderUpdate={ value => this.updateSymptoms( value, symptom.StressLevel ) }
                title="Stress level"
                leftText="No stress"
                rightText="Big stress"
              />
              <AddMealBox
                title="Did you eat anything outside the challenge diet?"
                placeholder="What did you eat?"
                parentObject={ this }
              />
              <button className="Symptoms__button" onClick={() => {
                this.update()
              }}>
                SUBMIT
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default DietTriggers
