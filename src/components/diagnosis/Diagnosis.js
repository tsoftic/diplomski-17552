import React, { Component } from 'react'

import '../../scss/DiagnosisContainer.css'
import '../../scss/Diagnosis.css'
import '../../scss/RangeSlider.css'
import Stepper from '../stepper/Stepper'
import step1Photo from '../../photos/step1Photo.png'
import step2Photo from '../../photos/step2Photo.png'
import step3Photo from '../../photos/step3Photo.png'
import step4Photo from '../../photos/step4Photo.png'
import step5Photo from '../../photos/step5Photo.png'
import { SymptomSlider } from '../common'
import { SecondExtraData } from './DiagnosisData'
import { getDate } from '../../utilities/date'

class Diagnosis extends Component {
  constructor() {
    super()
    this.state = {
      value: 10,
      checked1: false,
      checked2: false,
      checked3: false,
      checked4: false, }

    this.handleChangeSwitch1 = this.handleChangeSwitch1.bind( this )
    this.handleChangeSwitch2 = this.handleChangeSwitch2.bind( this )
    this.handleChangeSwitch3 = this.handleChangeSwitch3.bind( this )
    this.handleChangeSlider = this.handleChangeSlider.bind( this )
    this.handleOtherCondition = this.handleOtherCondition.bind( this )
    this.postAnswer = this.postAnswer.bind( this )
  }

  handleChangeSwitch1( event ) {
    this.props.irratableBowelSyndrome( event.target.checked )
  }
  handleChangeSwitch2( event ) {
    this.props.celiacDisease( event.target.checked )
  }
  handleChangeSwitch3( event ) {
    this.props.irratableBowelDisease( event.target.checked )
  }
  handleOtherCondition( event ) {
    const target = event.target
    const value = target.value
    const name = target.name

    if( name === "condition" ) {
      this.props.otherCondition( value )
    }

  }
  handleChangeSlider( value ) {
    this.props.sliderChange( value )
  }
  postAnswer( answers ) {
    let inquryResult = this.props.inquiry
    let answer = {
      diagnosed: answers[ 0 ].value,
      irritableBowelSyndrome: inquryResult.irratableBowelSyndrome,
      celiacDisease: inquryResult.celiacDisease,
      irritableBowelDisease: inquryResult.irritableBowelDiease,
      otherCondition: inquryResult.otherConditionValue,
      anyOfListedSymptoms: answers[ 2 ].value,
      currentCondition: Number.parseInt( inquryResult.value,10 ),
      uid: this.props.uid,
      date: getDate()
    }

    this.props.save( answer )
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }

  render() {
    const mockData = [
      {
        key: 1,
        title: 'Have you been diagnosed with a digestive issue?',
        image: step1Photo,
        continue: 'false',
      }, {
        key: 2,
        extraData: (
          <SecondExtraData
            { ...this.props.inquiry }
            handleChangeSwitch1={ this.handleChangeSwitch1 }
            handleChangeSwitch2={ this.handleChangeSwitch2 }
            handleChangeSwitch3={ this.handleChangeSwitch3 }
            handleOtherCondition={ this.handleOtherCondition }
            otherConditionValue={ this.props.otherConditionValue }
          />
        ),
        title: 'What medical issue/s have you been diagnosed with?',
        image: step2Photo,
        continue: 'true',
      }, {
        key: 3,
        extraData: (
          <div className="Diagnosis__symptoms">
            <label>Fever/chills</label>
            <label>Nausea/vomiting</label>
            <label>Light headedness</label>
            <label>Severe abdominal pain</label>
          </div>
        ),
        title: 'Do you have any of the following symptoms?',
        image: step3Photo,
        continue: 'false',
      }, {
        key: 4,
        extraData: (
          <SymptomSlider onSliderUpdate={ this.handleChangeSlider } />
        ),
        title: 'How would you rate your current condition?',
        image: step4Photo,
        continue: 'true',

      }, {
        key: 5,
        title: 'You are ready for Diet!',
        image: step5Photo,
        continue: 'true',
      },
    ]
    return (
      <div className="DiagnosisContainer footerSpacer">
        <Stepper
          data={ mockData }
          onStepperEnd={ answers => this.postAnswer( answers ) }
          finalText="We're done with this for now. Thank you!"
          history={ this.props.history }
        />
      </div>
    )
  }

}

export default Diagnosis
