import React from 'react'

const SecondExtraData = ( {
  irratableBowelSyndrome,
  celiacDisease,
  irritableBowelDiease,
  otherConditionValue,
  handleChangeSwitch1,
  handleChangeSwitch2,
  handleChangeSwitch3,
  handleOtherCondition,
} ) => (
  <div className="Issue">
    <div className="Issue__single">
      <label>Irratable Bowel Syndrome</label>
      <input
        className="Issue__checkbox"
        name="male"
        type="checkbox"
        checked={ irratableBowelSyndrome }
        onChange={ handleChangeSwitch1 }
      />
    </div>

    <div className="Issue__single">
      <label>Celiac Disease</label>
      <input
        className="Issue__checkbox"
        name="male"
        type="checkbox"
        checked={ celiacDisease }
        onChange={ handleChangeSwitch2 }
      />
    </div>

    <div className="Issue__single">
      <label>Inflammatory Bowel Disease</label>
      <input
        className="Issue__checkbox"
        name="male"
        type="checkbox"
        checked={ irritableBowelDiease }
        onChange={ handleChangeSwitch3 }
      />
    </div>

   
    <div className="Issue__single Issue__input">
      <label className="issue-header"> Condition not listed?</label>
      <input
        className="Field_Shape placeholder font"
        type="text"
        name="condition"
        placeholder="Enter condition"
        value={ otherConditionValue }
        onChange={ handleOtherCondition }
      />
    </div>
  </div>
)


export { SecondExtraData }
