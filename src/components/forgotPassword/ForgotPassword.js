import React, { Component } from 'react'
import FooterLogin from '../common/FooterLogin'
import '../../scss/ResetPassword.css'
import '../../scss/Common.css'

class ForgotPassword extends Component {

  constructor( props ) {
    super( props )
    this.state = {
      email: ''
    }

    this.handleChange = this
      .handleChange
      .bind( this )
    this.goToLogin = this
      .goToLogin
      .bind( this )
  }

  componentWillMount() {
    this.props.resetProps.resetMessage()
  }

  goToLogin() {
    this
      .props
      .resetProps
      .history
      .push( "/login" )
  }

  handleChange( event ) {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState( { [ name ]: value } )
  }

  resetPassword( event ) {
    event.preventDefault()
    this
      .props
      .resetProps
      .resetPassword( this.state.email )
  }

  render() {
    return (
      <div className="loginDiv font">
        <div className="reset">
          <label className="title">Reset Password</label>
          <p className="text">Enter your account email address<br/>
            and we&rsquo;ll send you a new password
          </p>
          <p className="signInEmail">ACCOUNT EMAIL ADDRESS</p>
          <form onSubmit={ ( event ) => this.resetPassword( event ) }>
            <input
              className="Field_Shape placeholders font"
              type="text"
              name="email"
              placeholder="Email address"
              value={ this.state.email }
              onChange={ this.handleChange }
            />
            <br/>
            <input
              className="resetPass pointer font"
              type="submit"
              name="submit"
              value="RESET PASSWORD"
              disabled={ this.props.resetProps.linkSent }
            /> {!this.props.resetProps.linkSent && (
              <div className="errorMessage">
                {this.props.resetProps.message}
              </div>
            )}
            {this.props.resetProps.linkSent && (
              <div>
                <p className="message">An email with password reset instructions<br/>
                  has been sent to your email address, if it<br/>
                  exists on our system.
                </p>
              </div>
            )}
          </form>
          <div className="belowForm">
            <label>REMEMBER YOUR PASSWORD?</label>
            <a className="signUp" name="linkRegistracija" onClick={ this.goToLogin }>SIGN IN NOW</a>
          </div>
        </div>
        <FooterLogin/>
      </div>
    )
  }
}

export default( ForgotPassword )
