import React, { Component } from 'react'
import '../../scss/ChallengeWeeks.css'
import '../../scss/DashboardGrid.css'


class challengeWeeks extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      challengeWeeks: [],
      hasScrolledTop:false
    }
    this.redirect = this.redirect.bind( this )
  }

  componentWillMount() {
    this.props.getAllChallengeWeeks()
  }
  componentWillReceiveProps( nextProps ) {
    if(nextProps.challengeWeeks){
      this.setState( { challengeWeeks: nextProps.challengeWeeks } )
      if(!this.state.hasScrolledTop){
        this.setState({hasScrolledTop:true})
        window.scrollTo(0,0);
      }
    }
  }
  redirect( weekId,title ) {
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){
      this.props.history.push( `/dayMeals/${title}/${ weekId }` )
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }
  renderWeek = ( week ) => {
    console.log("tu")
    if(week.userId===undefined){
      console.log(week.id,"hhhhh")
      return (
        <div key={week.id} style={{ backgroundImage: "url(" + week.picture  + ")"}} className="ChallengeWeeks__Single">
          <h1> {week.title}</h1>
          <span> {week.description}</span>
          <button className="ChallengeWeeks__Single--button" onClick={ () => this.redirect( week.id,week.title ) }>START WEEK</button>
        </div>
      )
    }
    if(week.userId===this.props.uid){
      return (
        <div key={week.id} style={{ backgroundImage: "url(" + week.picture  + ")"}} className="ChallengeWeeks__Single">
          <h1> {week.title}</h1>
          <span> {week.description}</span>
          <button className="ChallengeWeeks__Single--button" onClick={ () => this.redirect( week.id,week.title ) }>START WEEK</button>
        </div>
      )
    }
  }
  render() {
    return (
      <div className="ChallengeWeeks DashboardGrid"> 
        <h1>Carola Diet</h1>
        <h5>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
          Eget gravida cum sociis natoque penatibus et magnis. Ut tellus elementum sagittis vitae et leo duis ut diam.
          <br/><br/> 
          Vestibulum mattis ullamcorper velit sed ullamcorper. Nullam ac tortor vitae purus faucibus ornare suspendisse 
          sed. Vitae turpis massa sed elementum tempus egestas sed. 
          <br/><br/>
          Arcu risus quis varius quam. Lobortis feugiat vivamus at augue eget arcu dictum. Ac feugiat sed lectus vestibulum mattis.
          Tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum. Ipsum nunc aliquet bibendum enim. 
        </h5>
        <span>
        </span>
        <div>
        {
          this.state.challengeWeeks.map( week => this.renderWeek( week ) )
        }
          </div>
      </div>
    )
  }

}

export default challengeWeeks
