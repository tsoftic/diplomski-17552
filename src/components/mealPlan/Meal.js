import React, { Component } from 'react'
import YouTube from 'react-youtube'
import { BreadcrumbsMeal } from '../../components/common'
import '../../scss/Meal.css'
import '../../scss/DashboardGrid.css'

import timePhoto from '../../photos/TimeMeal.png'
import makesPhoto from '../../photos/MakesMeal.png'

class Meal extends Component {
  constructor( props ) {
    super( props )

    this.state = {
      title:'',
      mealProps: { ingredients: [], directions: [] },
      videoPlayed: false,
      hasScrolledTop:false
    }
    
    this.Video = this.Video.bind( this )
  }
  componentWillMount() {
    this.props.selectMeal( this.props.params.id )
  }
  componentWillReceiveProps( nextProps ) {
    if( nextProps.mealProps != null ){
      this.setState( { mealProps: nextProps.mealProps, title: this.props.params.weektitle  } )
      if(!this.state.hasScrolledTop){
        this.setState({hasScrolledTop:true})
        window.scrollTo(0,0);
      }
    }
  }
  _onReady( event ) {
    event.target.pauseVideo()
  }
  Video(){
    if(!this.state.videoPlayed){
      const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
      if (user_track.uid){
        this.setState({videoPlayed:true})
      }
      else{
        window.localStorage.removeItem('carola');
        this.props.history.push( "/login" )
      }
    }
  }
  render() {
    const opts = {
      height: 500,
      width: 800
    }
    const opts2={
      height: 200,
      width:350
    }
    return (
      <div className="Meal DashboardGrid">
        <BreadcrumbsMeal { ...this.state } />
        <h1 className="Meal__title">{this.state.mealProps.title}</h1>
        <div className="Meal__Photo">
          <img src={ this.state.mealProps.mealImgUrl }alt= "Img"/>
        </div>
        <div className="Meal__ContextWrapper">
          <div className="Meal__IngredientsAndDirections">
            <h2 className="Meal__IngredientsAndDirections--title"> INGREDIENTS</h2>
            <div>
              {this.state.mealProps.ingredients.map( function( ingredient, index ) {
                return <li key={ index }>{ingredient}</li>
              } )}
            </div>
            <h2 className="Meal__IngredientsAndDirections--title"> DIRECTIONS</h2>
            <div>
              {
                this.state.mealProps.directions.map( function( direction, index ) {
                  return <li key={ index }>{index + 1}. {direction}</li>
                } )}
            </div>
          </div>
          <div className="Meal__TimeAndMakes">
            <div className="Meal__TimeAndMakes--Single">
              <img  src={ timePhoto } alt= "Img"/>
              <div  className="Meal__TimeAndMakes--Single--data">
                <h2>Time</h2>
                <label>Ready in {this.state.mealProps.time} minutes</label>
              </div>
            </div>
            <div className="Meal__TimeAndMakes--Single">
              <img  src={ makesPhoto } alt= "Img"/>
              <div  className="Meal__TimeAndMakes--Single--data">
                <h2>Makes</h2>
                <label>{this.state.mealProps.servingNumber} Serving</label>
              </div>
            </div>
          </div>
        </div>
        <div className="Meal_Video">
          <h2 style={ { marginBottom: 20 } }className="Meal__IngredientsAndDirections--title"> HOW TO PREPARE THIS MEAL (VIDEO)</h2>
          <div>
            <YouTube
              videoId={ this.state.mealProps.prepareVideoUrl }
              opts={window.innerWidth < 800 ? opts2 : opts }
              onReady={ this._onReady }
              onPlay={this.Video}
            />
          </div>
        </div>
      </div>
    )
  }

}

export default Meal
