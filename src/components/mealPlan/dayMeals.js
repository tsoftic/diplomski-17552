import React, { Component } from 'react'
import { BreadcrumbsMeal } from '../../components/common'
import '../../scss/ChallengeWeeks.css'
import '../../scss/DayMeals.css'
import '../../scss/DashboardGrid.css'

import Arrow from '../../photos/ArrowMeal.png'
import MealPreparationTag from '../../photos/MealPreparationTag.png'
import BreakfastTag from '../../photos/BreakfastTag.png'
import SnackTag from '../../photos/SnackTag.png'
import LunchTag from '../../photos/LunchTag.png'
import DinnerTag from '../../photos/DinnerTag.png'
import emailLogo from '../../photos/email-icon.png'

class dayMeals extends Component {

  constructor( props ) {
    super( props )

    this.state = {
      challengeWeek:{},
      dailyMeals: [],
      mealPreparationFilter:true,
      individualMealFilter:true,
      ingredientsEmailed:1,
      hasScrolledTop:false,
    }

    this.redirect = this.redirect.bind( this )
    this.renderDay = this.renderDay.bind( this )
    this.IngredientsEmailed = this.IngredientsEmailed.bind( this )
  }
  IngredientsEmailed(){
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    if (user_track.uid){  
      
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }
  }
  componentWillMount() {
      this.props.getChallengeWeek( this.props.params.weekid )
    }
  componentWillReceiveProps( nextProps ) {
    if( nextProps.challengeWeek && nextProps.challengeWeek.dailyMealsModel ){
      this.setState( { challengeWeek: nextProps.challengeWeek.challengeWeekModel, dailyMeals: nextProps.challengeWeek.dailyMealsModel } )
      if(!this.state.hasScrolledTop){
        this.setState({hasScrolledTop:true})
        window.scrollTo(0,0);
      }
    }
  }

  redirect( mealId,title ) {
    this.props.history.push( `/meal/${this.props.params.weektitle}/${title}/${ mealId }` )
  }
  renderDay( dayMeal ) {
    return(
      <div style={{marginBottom:-20}}>
        {this.renderDayTag( dayMeal.day )}
        {
          this.state.individualMealFilter &&
          <div className="DayMeals__Day">
            {this.renderMeal( dayMeal ) }
          </div>
        }
        {
          this.state.mealPreparationFilter &&
          <div className="DayMeals__Day">
              {this.renderMealPreparation(dayMeal)}
          </div>
        }
      </div>
    )
  }
  renderMeal( dayMeal ) {
    var daysMeals={
      "Breakfast" : 0,
      "Snack" : 1,
      "Lunch" : 2,
      "Dinner" : 3
    }
    function compare(a,b){
      if(a===b){
        return 0;
      }
      return a < b ? -1:1;
    }
    return dayMeal.meals.sort((a, b) => {
        var result = compare(daysMeals[a.lunchType],daysMeals[b.lunchType])
        return result;
      	}).map( ( meal ) => {
      return (
        (!meal.mealPreparation) && (
        <div className="DayMeals__SingleMeal">
          <div onClick={ () => this.redirect( meal.id,meal.title ) } style={{ backgroundImage: "url(" + meal.mealImgUrl  + ")"}} className="DayMeals__SingleMeal--Photo">
          </div>
          <div className="DayMeals__SingleMeal--Content">
            {this.renderTag(meal.lunchType)}
            <h2 onClick={ () => this.redirect( meal.id,meal.title ) }>{meal.title}</h2>
            <div  className="DayMeals__SingleMeal--Content--span">{meal.description}</div>
          </div>
          <div className="DayMeals__SingleMeal--Photo--Arrow">
            <img src={ Arrow } onClick={ () => this.redirect( meal.id,meal.title ) } alt="img"/>
          </div>
        </div>)
      )
    } )
  }
  renderMealPreparation( dayMeal ) {
    var daysMeals={
      "Breakfast" : 0,
      "Snack" : 1,
      "Lunch" : 2,
      "Dinner" : 3
    }
    function compare(a,b){
      if(a===b){
        return 0;
      }
      return a < b ? -1:1;
    }
    return dayMeal.meals.sort((a, b) => {
      var result = compare(daysMeals[a.lunchType],daysMeals[b.lunchType])
      return result;
      }).map( ( meal ) => {
      return (
        meal.mealPreparation && (
        <div className="DayMeals__SingleMeal">
          <div onClick={ () => this.redirect( meal.id,meal.title ) } style={{ backgroundImage: "url(" + meal.mealImgUrl  + ")"}}  className="DayMeals__SingleMeal--Photo">
          </div>
          <div className="DayMeals__SingleMeal--Content ">
            <img className="DayMeals__Tag" src={ MealPreparationTag }/>
            <h2 onClick={ () => this.redirect( meal.id,meal.title ) } >{meal.title}</h2>
            <div  className="DayMeals__SingleMeal--Content--span">{meal.description}</div>
          </div>
          <div className="DayMeals__SingleMeal--Photo--Arrow">
            <img src={ Arrow } onClick={ () => this.redirect( meal.id,meal.title ) } alt="img"/>
          </div>
        </div>)
        
      )
    } )
  }
  
  handleCheckboxChange=({ target })=>{
    const { name } = target
    let newState = {}

    if (name === 'all') newState = {mealPreparationFilter:true, individualMealFilter:true}
    if ( name === 'individual') newState = {mealPreparationFilter:false,individualMealFilter:true}
    if ( name === 'mealPrep') newState = {mealPreparationFilter:true,individualMealFilter:false}

    this.setState( { ...newState } )
    const user_track = JSON.parse( localStorage.getItem( 'carola' ) )
    const allMeals=(newState.mealPreparationFilter && newState.individualMealFilter);
    const individualMeals=(!newState.mealPreparationFilter && newState.individualMealFilter);
    const mealPreparationMeals=(newState.mealPreparationFilter && !newState.individualMealFilter);
    if (user_track.uid){
      
    }
    else{
      window.localStorage.removeItem('carola');
      this.props.history.push( "/login" )
    }


  }
  onDropdownChangeEmail = (e) => {
    this.setState({ingredientsEmailed: e.target.value});
  }
  renderSideContent(){
    return(
      <div className="DayMeals__SideContent">
        <div className="DayMeals__SideContent--Mail">
          <div className="DayMeals__SideContent--Mail__email">
            <img src={ emailLogo } className="DayMeals__SideContent--Mail__picture"alt="img"/> 
            <label> Email Shopping List </label>
          </div>
          <span>
            Make sure you purchase your items
            every week before meal preparation
          </span>
          <div className="DayMeals__SideContent--Mail--select">
            <select onChange= {this.onDropdownChangeEmail}>
              <option value="1"> Email Shopping List for one </option>
              <option value="2"> Email Shopping List for two </option>
              <option value="3"> Email Shopping List for three </option>
              <option value="4"> Email Shopping List for four</option>
            </select>
          </div>
          <div className="DayMeals__SideContent--Mail--button">
            <a onClick={ () => this.IngredientsEmailed() } target="_blank">
              GET WEEKLY INGREDIENTS
            </a>
          </div>
        </div>
        <div className="DayMeals__SideContent--Filter">
          <h4>Filters</h4>
          <div>
            <input
                  className="DayMeals__SideContent--Filter--checkbox"
                  name="all"
                  type="radio"
                  ref="all"
                  checked={this.state.mealPreparationFilter && this.state.individualMealFilter}
                  onChange={ this.handleCheckboxChange}
                />
            <label>All meals</label>
          </div>
          <div>
            <input
                  className="DayMeals__SideContent--Filter--checkbox"
                  name="individual"
                  type="radio"
                  checked={!this.state.mealPreparationFilter && this.state.individualMealFilter}
                  onChange={ this.handleCheckboxChange}
                />
            <label>Individal meals</label>
          </div>
          <div>
            <input
                  className="DayMeals__SideContent--Filter--checkbox"
                  name="mealPrep"
                  type="radio"
                  checked={this.state.mealPreparationFilter && !this.state.individualMealFilter}
                  onChange={ this.handleCheckboxChange}
                />
            <label>Meal preparation</label>
            </div>
          </div>
      </div>
    )
  }
  renderWeek( week ) {
    return (
        <div style={{ backgroundImage: "url(" + week.picture  + ")"}}className="DayMeals__ChallengeWeek">
          <div>
            <h1> {week.title}</h1>
            <span> {week.description}</span>
          </div>
        </div>
    )
  }
  renderDayTag( day ) {
    var Day
    switch( day ) {
      case 0:
        Day = "Monday"
        break
      case 1:
        Day = "Tuesday"
        break
      case 2:
        Day = "Wednesday"
        break
      case 3:
        Day = "Thursday"
        break
      case 4:
        Day = "Friday"
        break
      case 5:
        Day = "Saturday"
        break
      case 6:
        Day = "Sunday"
        break
      default:
        console.warn( 'invalid day' )
        break
    }
    return(
      <h2 className="DayMeals__DayHeading"> {Day} </h2>
    )
  }

  renderTag( tag ) {
    var Tag
    switch( tag.toLowerCase() ) {
      case "breakfast":
        Tag = BreakfastTag
        break
      case "snack":
        Tag = SnackTag
        break
      case "lunch":
        Tag = LunchTag
        break
      case "dinner":
        Tag = DinnerTag
        break
      default:
        console.warn( 'invalid day' )
        break
    }
    return <img className="DayMeals__Tag" src={ Tag }/>
  }
  render() {
    return (
      <div className="DayMeals DashboardGrid">
        <BreadcrumbsMeal { ...this.state } />
        {this.renderWeek(this.state.challengeWeek)}
        <div className="DayMeals__Wrapper">
        <div className="DayMeals__Wrapper--meal">
          {
            this.state.dailyMeals.sort((a, b) => a.day > b.day).map( ( dayMeal ) => {
              return this.renderDay( dayMeal )
          } )}
        </div>
          <div className="DayMeals__Wrapper--side">
            {this.renderSideContent()}
          </div>
        </div>
      </div>
    )
  }

}

export default dayMeals
