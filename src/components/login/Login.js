import React, { Component } from 'react'
import firebase from 'firebase'
import logo from '../../photos/carolaLogo.png'
import googlelogo from '../../photos/googlelogo.png'
import '../../scss/Login.css'
import '../../scss/Common.css'
import FooterLogin from '../common/FooterLogin'

class Login extends Component {

  constructor( props ) {
    super( props )
    if ( localStorage.getItem( 'carola' ) ) {
      this.redirectAfterLogin()
    }

    this.state = {
      uid: '',
      erroMsg: '',
      showSignUp: false,
      showForgotPassword: false,
      emailValidation: false
    }

    this.goToRegister = this
      .goToRegister
      .bind( this )
    this.goToForgotPassword = this
      .goToForgotPassword
      .bind( this )
    this.handleSubmit = this.handleSubmit.bind( this )
    this.redirectAfterLogin = this.redirectAfterLogin.bind( this )
  }

  componentWillMount() {
    this.props.loginProps.resetMessage()
  }

  goToRegister() {
    this
      .props
      .loginProps
      .history
      .push( "/register" )
  }

  goToForgotPassword() {
    this
      .props
      .loginProps
      .history
      .push( "/forgotPassword" )
  }

  redirectAfterLogin() {
    return this
      .props
      .loginProps
      .history
      .push( "/dashboard" )
  }

  handleSubmit( event ) {
    this.props.loginProps.authWithEmailPassword( this.emailInput.value, this.passwordInput.value,this.redirectAfterLogin )
    event.preventDefault()
  }

  renderLogin() {
    return (
      <div className="loginDiv font">
        <div className="login">
          <img className="photo" src={ logo } alt="photo"/>
          <label className="title">Welcome to Carola</label>
          
          <p className="signInSocialMedia">SIGN IN WITH SOCIAL MEDIA</p>
          <button
            className="google"
            onClick={ () => this
              .props
              .loginProps
              .authenticate( new firebase.auth.GoogleAuthProvider(),this.redirectAfterLogin ) }
          >
            <img src={ googlelogo } alt="photo"/>
            <label className="pointer">GOOGLE</label>
          </button>
          <form onSubmit={ this.handleSubmit }>
            <p className="signInEmail">SIGN IN WITH EMAIL</p>
            <input
              className="Field_Shape placeholders font"
              type="text"
              name="username"
              placeholder="Email address"
              ref={ ( input ) => {
                this.emailInput = input
              } }
            />
            <br/>
            <input
              className="Field_Shape placeholders font"
              type="password"
              name="password"
              placeholder="Password"
              ref={ ( input ) => {
                this.passwordInput = input
              } }
            />
            <br/>
            <input
              className="signIn pointer font"
              type="submit"
              name="submit"
              value="SIGN IN"
            />
            {!( this.props.loginProps.message === '' ) && (
              <div className="errorMessage">
                {this.props.loginProps.message}
              </div>
            )}
          </form>
          <div className="belowForm">
            <label>DON&rsquo;T HAVE AN ACCOUNT?</label>
            <a className="signUp" name="linkRegistracija" onClick={ this.goToRegister }>SIGN UP</a>
            <br/>
            <label>FORGOT PASSWORD?</label>
            <a className="signUp" name="linkRegistracija" onClick={ this.goToForgotPassword }>RESET NOW</a>
          </div>
        </div>
        <FooterLogin/>
      </div>

    )
  }

  render() {
    return this.renderLogin()
  }
}

export default( Login )
