import firebase from 'firebase'
import {
  INQUIRY_SAVE_SUCCESS,
  INQUIRY_SAVE_FAILURE,
  ADD_VALUE_IRRATABLEBOWELSYNDROM,
  ADD_VALUE_CELIACDISEASE,
  ADD_VALUE_IRRATABLEBOWELDISEASE,
  ADD_VALUE_OTHERCONDITION,
  ADD_VALUE_SLIDER,
  LOAD_INQUIRY_SUCCESS,
  LOAD_INQUIRY_FAILURE
} from './types'

export const irratableBowelSyndrome = ( value ) => ( dispatch ) => {
  dispatch( { type: ADD_VALUE_IRRATABLEBOWELSYNDROM, payload: value } )
}

export const celiacDisease = ( value ) => ( dispatch ) => {
  dispatch( { type: ADD_VALUE_CELIACDISEASE, payload: value } )
}

export const irratableBowelDisease = ( value ) => ( dispatch ) => {
  dispatch( { type: ADD_VALUE_IRRATABLEBOWELDISEASE, payload: value } )
}


export const otherCondition = ( value ) => ( dispatch ) => {
  dispatch( { type: ADD_VALUE_OTHERCONDITION, payload: value } )
}

export const sliderChange = ( value ) => ( dispatch ) => {
  dispatch( { type: ADD_VALUE_SLIDER, payload: value } )
}

export const save = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'users/'+payload.uid + '/inquiries/' + payload.date )
    .set( { inquiry: payload } )
    .then( () => {
      dispatch( { type: INQUIRY_SAVE_SUCCESS, payload: true } )
    } )
    .catch( ( error ) => {
      dispatch( { type: INQUIRY_SAVE_FAILURE, payload: error.message } )
    } )

}

export const loadInquiry = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'users/'+payload.uid + '/inquiries' )
    .once( "value", ( snapshot ) => {
      dispatch( { type: LOAD_INQUIRY_SUCCESS, payload: snapshot.val() } )
    } )
    .catch( ( error ) => {
      dispatch( { type: LOAD_INQUIRY_FAILURE, payload: error.message } )
    } )
}
