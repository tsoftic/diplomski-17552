import firebase from 'firebase'
import { TRIGGERS_SAVE_SUCCESS, TRIGGERS_SAVE_FAILURE, SYMPTOMS_SAVE_SUCCESS, SYMPTOMS_SAVE_FAILURE, LATEST_TRIGGER, UPDATED_TRIGGER } from './types'
import { getDate, addMinutes, getMaxDate } from '../utilities/date.js'
import { triggerFood } from "../utilities/enum"
import moment from "moment-timezone"

//#region Triggers

export const update = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'triggers/' + payload.uid )
    .update( { trigger: payload } )
    .then( () => {
      dispatch( { type: TRIGGERS_SAVE_SUCCESS, payload: true } )
    } )
    .catch( ( error ) => {
      dispatch( { type: TRIGGERS_SAVE_FAILURE, payload: error.message } )
    } )
}

export const loadTrigger = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'users/'+payload + '/triggers' )
    .once( "value", ( snapshot ) => {
      let dateArray = []
      snapshot.forEach( function ( child ) {
        dateArray.push( Date.parse( child.key ) )
      } )
      let maxDate = getMaxDate( dateArray )
      if ( dateArray.length === 0 ) {
        insertTrigger(payload,dispatch)
      }
      else{
        firebase.database().ref( 'users/'+ payload + '/triggers/' + moment(maxDate.toString()).clone().tz("America/Whitehorse").toString()+ '/trigger' ).once( 'value', ( snapshot ) => {
          dispatch( { type: LATEST_TRIGGER, payload: snapshot.val() } )
        } )
      }
    } )
}

export const updateTrigger = ( title, value ) => ( dispatch ) => {
  let payload = {
    title: title,
    value: value
  }
  dispatch( { type: UPDATED_TRIGGER, payload: payload } )
}

export const insertTrigger = ( payload,dispatch) => {
  let baseTriggers ={
    pasta: false,
    meat: false,
    seafood: false,
    diaryProducts: false,
    vegetables: false,
    fruits: false
  }
  let date= moment(payload.date).clone().tz("America/Whitehorse").toString()
  firebase
    .database()
    .ref( 'users/'+ payload + '/triggers/' + date)
    .set( { trigger: baseTriggers } )
    .then( ( result ) => {
      dispatch( { type: TRIGGERS_SAVE_SUCCESS, payload: true } )
    } )
    .catch( ( error ) => {
      dispatch( { type: TRIGGERS_SAVE_FAILURE, payload: error.message } )
    } )
}
//#endregion Triggers

//#region Symptoms
export const addDefaultSymptoms = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'users/'+payload.uid + '/symptoms' )
    .once( "value", ( snapshot ) => {
      let dateArray = []
      snapshot.forEach( function ( child ) {
        dateArray.push( Date.parse( child.key ) )
      } )
      let maxDate = getMaxDate( dateArray )
      if ( dateArray.length === 0 || getDate() >= addMinutes( Date.parse( maxDate ), 5 ) ) {
        insertSymptom( payload, dispatch )
      } else {
        updateSymptom( payload, dispatch )
      }
    } )
}

function updateSymptom (payload, dispatch) {
  firebase
    .database()
    .ref( 'users/'+payload.uid + '/symptoms' )
    .once( "value", ( snapshot ) => {
      let dateArray = []
      snapshot.forEach( function ( child ) {
        dateArray.push( Date.parse( child.key ) )
      } )

      if ( dateArray.length !== 0 ) {
        let maxDate = getMaxDate( dateArray )
        let date= moment(maxDate).clone().tz("America/Whitehorse").toString()
        let ref = firebase.database().ref( 'users/'+payload.uid + '/symptoms/' + date )

        ref
          .update( { symptom: payload } )
          .then( () => {
            dispatch( { type: SYMPTOMS_SAVE_SUCCESS, payload: true } )
          } )
          .catch( ( error ) => {
            dispatch( { type: SYMPTOMS_SAVE_FAILURE, payload: error.message } )
          } )
      }
    } )
}

function insertSymptom (payload,dispatch) {
  let date= moment(payload.date).clone().tz("America/Whitehorse").toString()
  firebase
    .database()
    .ref( 'users/'+payload.uid + '/symptoms/' + date )
    .set( { symptom: payload } )
    .then( ( result ) => {
      dispatch( { type: SYMPTOMS_SAVE_SUCCESS, payload: true } )
    } )
    .catch( ( error ) => {
      dispatch( { type: SYMPTOMS_SAVE_FAILURE, payload: error.message } )
    } )
}
//#endregion Symptoms
