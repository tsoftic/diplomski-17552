/*------------------- TEST REDUCER --------------------*/
export const TEST_REDUCER_ACTION = 'test_reducer_action'

/*----------------------- LOGIN -------------------------*/
export const LOGIN_USER_REQUEST = 'login_user_request'
export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAILURE = 'error_message'
export const ADD_USER_ID = 'add_user_id'
export const REMOVE_USER_ID = 'remove_user_id'
/*---------------------- REGISTER -----------------------*/
export const REGISTER_USER_SUCCESS = 'register_user_success'
export const REGISTER_USER_FAILURE = 'error_message'

/*------------------- RESET PASSWORD --------------------*/
export const RESETPASS_USER_SUCCESS = 'resetpass_user_success'
export const RESETPASS_USER_FAILURE = 'error_message'
export const CHANGE_PASSWORD_SUCCESS = 'change_password_success'
export const CHANGE_PASSWORD_FAILURE = 'change_password_failure'

/*---------------------- RESET MESSAGE -----------------------*/
export const RESETMESSAGE_ERROR_MESSAGE = 'error_message_reset'
/*---------------------- PROFILE SETTINGS --------------------*/
export const LOAD_PROFILE_SETTINGS = 'load_profile_settins'
export const LOAD_PROFILE_FAILURE = 'load_profile_failure'
export const UPDATE_USER_PROFILE = 'update_user_profile'
export const UPDATE_USER_PROFILE_FAILURE = 'update_user_profile_failure'
export const LOAD_EMAIL_PREFERENCES = 'load_email_preferences'
export const LOAD_EMAIL_PREFERENCES_FAILURE = 'load_email_preferences_failure'
export const UPDATE_EMAIL_PREFERENCES = 'update_email_preferences'
export const UPDATE_EMAIL_PREFERENCES_FAILURE = 'update_email_preferences_failure'
export const UPDATED_PREFERENCE = 'updated_preference'

/*---------------------- INQUIRY -----------------------*/
export const INQUIRY_SAVE_SUCCESS = 'inquiry_save_success'
export const INQUIRY_SAVE_FAILURE = 'inquiry_save_failure'
export const ADD_VALUE_CELIACDISEASE = 'add_value_celiacdisease'
export const ADD_VALUE_IRRATABLEBOWELSYNDROM = 'add_value_irratablebowelsyndrome'
export const ADD_VALUE_IRRATABLEBOWELDISEASE = 'add_value_irratableboweldisease'
export const ADD_VALUE_OTHERCONDITION = 'add_value_othercondition'
export const ADD_VALUE_SLIDER = 'add_value_slider'
export const LOAD_INQUIRY_SUCCESS = 'load_inquiry_success'
export const LOAD_INQUIRY_FAILURE = 'load_inquiry_failure'

/*--------------------- DASHBOARD ----------------------*/
export const TRIGGERS_SAVE_SUCCESS = 'triggers_save_success'
export const TRIGGERS_SAVE_FAILURE = 'triggers_save_failure'
export const ADD_TRIGGER_ID = 'add_trigger_id'
export const LATEST_TRIGGER = 'latest_trigger'
export const UPDATED_TRIGGER = 'updated_trigger'
export const SYMPTOMS_SAVE_SUCCESS = 'symptoms_save_success'
export const SYMPTOMS_SAVE_FAILURE = 'symptoms_save_failure'

/*--------------------- MEAL ----------------------*/
export const SELECT_MEAL = 'select_meal'
export const GET_CHALLENGEWEEK = 'get_challengeweek'
export const GET_ALLCHALLENGEWEEKS = 'get_allchallengeweeks'