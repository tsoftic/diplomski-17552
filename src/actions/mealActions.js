import firebase from 'firebase'

import {
  SELECT_MEAL,
  GET_CHALLENGEWEEK,
  GET_ALLCHALLENGEWEEKS
} from './types'

export const selectMeal = ( mealId ) => dispatch => {

  firebase.database().ref( 'meals/' + mealId )
    .once( "value", ( snapshot1 ) => {
      dispatch( { type: SELECT_MEAL, payload: snapshot1.val() } )
    } )
}

export const getChallengeWeek = ( payload ) => ( dispatch ) => {
  var challengeWeek = {}
  var dailyMeals = []
  firebase
    .database()
    .ref( 'weekChallenges/' + payload + '/' )
    .once( "value", ( snapshot ) => {
      challengeWeek = snapshot.val()
      challengeWeek.dailyMeals.forEach( ( item ) => {
        firebase.database().ref( 'dayPlans/' + item.id + '/' )
          .once( "value", ( snapshot1 ) => {
            dailyMeals.push( snapshot1.val() )
            dispatch( { type: GET_CHALLENGEWEEK, payload: { challengeWeekModel: challengeWeek, dailyMealsModel: dailyMeals } } )
          } )
      } )

    } )
}


export const getAllChallengeWeeks = ( payload ) => ( dispatch ) => {
  
  console.log("tututut")
  var challengeWeeks = []
  console.log( firebase.database())
  firebase
    .database()
    .ref( 'weekChallenges' ).
    once( "value", ( snapshot ) => {
      snapshot.forEach( function( childSnapshot ) {
        var childData = childSnapshot.val()
        challengeWeeks.push( childData )
        
      } )
      dispatch( { type: GET_ALLCHALLENGEWEEKS, payload: challengeWeeks } )
    } )

    console.log(challengeWeeks,"ahhahaah")
}