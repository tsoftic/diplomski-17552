import * as firebase from 'firebase'
import {
  LOGIN_USER_FAILURE,
  REGISTER_USER_FAILURE,
  ADD_USER_ID,
  REMOVE_USER_ID,
  RESETPASS_USER_SUCCESS,
  RESETPASS_USER_FAILURE,
  RESETMESSAGE_ERROR_MESSAGE,
  LOAD_PROFILE_SETTINGS,
  LOAD_PROFILE_FAILURE,
  UPDATE_USER_PROFILE,
  UPDATE_USER_PROFILE_FAILURE,
  UPDATE_EMAIL_PREFERENCES,
  UPDATE_EMAIL_PREFERENCES_FAILURE,
  LOAD_EMAIL_PREFERENCES,
  LOAD_EMAIL_PREFERENCES_FAILURE,
  UPDATED_PREFERENCE,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
} from './types'
import { getMaxDate } from "./../utilities/date"
const saveToLocaleStorage = ( result ) => {
  window.localStorage.setItem( 'carola', JSON.stringify( { uid: result.user.uid, displayName: result.user.displayName ? result.user.displayName : 'John Doe', email: result.user.email } ) )
}

const saveToLocaleStorageAfterRegister = ( result ) => {
  window.localStorage.setItem( 'carola', JSON.stringify( { uid: result.uid, displayName: result.displayName ? result.displayName : 'John Doe', email: result.email } ) )
}

export const signOut = () => () => {
  logOut()
}

function logOut( dispatch ) {
  firebase
    .auth()
    .signOut()
    .then( () => {
      dispatch( { type: REMOVE_USER_ID, payload: null } )
    } )
}

export const authenticate = ( provider, callback ) => ( dispatch ) => {
  firebase
    .auth()
    .signInWithPopup( provider )
    .then( ( result ) => {
      updateUserId( result.user, dispatch )
      updateUserProfile( { uid: result.user.uid,
        firstName: result.additionalUserInfo.profile.given_name ? result.additionalUserInfo.profile.given_name : ( result.additionalUserInfo.profile.first_name ?  result.additionalUserInfo.profile.first_name : "" ),
        lastName: result.additionalUserInfo.profile.family_name ? result.additionalUserInfo.profile.family_name : ( result.additionalUserInfo.profile.last_name ?  result.additionalUserInfo.profile.last_name : "" ) }, dispatch )
      updateEmailPreference( result.user, dispatch )
      dispatch( { type: ADD_USER_ID, payload: result.user  } )
      saveToLocaleStorage( result )
      callback()
    } )
}

export const authWithEmailPassword = ( email, password, callback ) => ( dispatch ) => {
  // Implement validation
  firebase
    .auth()
    .signInWithEmailAndPassword( email, password )
    .then( ( result ) => {
      dispatch( { type: ADD_USER_ID, payload: { result } } )
      saveToLocaleStorage( { user: result } )
      callback()
    } )
    .catch( ( error ) => {
      dispatch( { type: LOGIN_USER_FAILURE, payload: error.message } )
    } )
}

export const registerUser = ( payload, callback ) => ( dispatch ) => {
  // Implement validation
  firebase
    .auth()
    .createUserWithEmailAndPassword( payload.email, payload.password )
    .then( ( result ) => {
      // update user's full name
      result.updateProfile( { displayName: payload.firstName + ' ' + payload.lastName } )
      payload.uid = result.uid
      //update user's profile
      updateUserId( payload, dispatch )
      updateUserProfile( payload, dispatch )
      updateEmailPreference( payload, dispatch )
      var data
      if ( result !== null && result.uid ) {
        data = {
          uid: result.uid,
          email: result.email,
          displayName: result.displayName !== null ? result.displayName : payload.firstName + ' ' + payload.lastName
        }
      }
      dispatch( { type: ADD_USER_ID, payload: result } )
      saveToLocaleStorageAfterRegister( data )
      callback()
    } )
    .catch( ( error ) => {
      dispatch( { type: REGISTER_USER_FAILURE, payload: error.message } )
    } )
}



function getLatestInquiryDetails( payload, callback ) {
  firebase
    .database()
    .ref( 'users/' + payload + '/inquiries' )
    .once( "value", ( snapshot ) => {
      let dateArray = []
      snapshot.forEach( function ( child ) {
        dateArray.push( Date.parse( child.key ) )
      } )
      let maxDate = getMaxDate( dateArray )
      if ( dateArray.length !== 0 ) {
        firebase.database().ref( 'users/' + payload + '/inquiries/' + maxDate + '/inquiry' ).once( 'value', ( snapshot ) => {
          callback( snapshot.val() )
        } )
      }
    } )
}

function getSymptomsDetails( payload, callback ) {
  firebase
    .database()
    .ref( 'users/' + payload + '/symptoms' )
    .once( "value", ( snapshot ) => {
      let dateArray = []
      snapshot.forEach( function ( child ) {
        dateArray.push( Date.parse( child.key ) )
      } )
      let maxDate = getMaxDate( dateArray )
      if ( dateArray.length !== 0 ) {
        firebase.database().ref( 'users/' + payload + '/symptoms/' + maxDate + '/symptom' ).once( 'value', ( snapshot ) => {
          let data = {
            numberOfLoggedSymptoms: dateArray.length,
            timeLoggedSymptom: maxDate,
            symptomData: snapshot.val()
          }
          callback( data )
        } )
      }
    } )
}
export const loadProfile = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'users/' + payload + '/profile/general' )
    .once( "value", ( snapshot ) => {
      dispatch( { type: LOAD_PROFILE_SETTINGS, payload: snapshot.val() } )
    } )
    .catch( ( error ) => {
      dispatch( { type: LOAD_PROFILE_FAILURE, payload: error.message } )
    } )
}

export const updateGeneralInfo = ( payload ) => ( dispatch ) => {
  updateUserProfile( payload, dispatch )
}

export const updateEmailPreferences = ( payload ) => ( dispatch ) => {
  updateEmailPreference( payload, dispatch )
}

export const loadEmailPreferences = ( payload ) => ( dispatch ) => {
  firebase
    .database()
    .ref( 'users/'+ payload + '/profile/emailPreferences' )
    .once( "value", ( snapshot ) => {
      dispatch( { type: LOAD_EMAIL_PREFERENCES, payload: snapshot.val() } ) /*change */
    } )
    .catch( ( error ) => {
      dispatch( { type: LOAD_EMAIL_PREFERENCES_FAILURE, payload: error.message } )
    } )
}

function updateUserId( payload, dispatch ) {
  firebase
    .database()
    .ref( 'users/' + payload.uid + '' )
    .set( {
      id: payload.uid,
    } )
    .then( () => { dispatch( { type: UPDATE_USER_PROFILE, payload: true } ) } )
    .catch( ( error ) => { dispatch( { type: UPDATE_USER_PROFILE_FAILURE, payload: error.message } ) } )

}

function updateUserProfile( payload, dispatch ) {
  firebase
    .database()
    .ref( 'users/' + payload.uid + '/profile/general' )
    .set( {
      firstName: payload.firstName,
      lastName: payload.lastName,
      gender: payload.gender ? payload.gender : "Unknown"
    } )
    .then( () => { dispatch( { type: UPDATE_USER_PROFILE, payload: true } ) } )
    .catch( ( error ) => { dispatch( { type: UPDATE_USER_PROFILE_FAILURE, payload: error.message } ) } )

}

function updateEmailPreference( payload, dispatch ) {
  firebase
    .database()
    .ref( 'users/' + payload.uid + '/profile/emailPreferences' )
    .set( {
      email: payload.email,
      allowNotifications: payload.allowNotifications ? payload.allowNotifications : false,
      newsletters: payload.newsletters ? payload.newsletters : false,
      dietChallengeStatus: payload.dietChallengeStatus ? payload.dietChallengeStatus : false
    } )
    .then( () => { dispatch( { type: UPDATE_EMAIL_PREFERENCES, payload: true } ) } )
    .catch( ( error ) => { dispatch( { type: UPDATE_EMAIL_PREFERENCES_FAILURE, payload: error.message } ) } )
}

export const updatePreference = ( title, value ) => ( dispatch ) => {
  let payload = {
    title: title,
    value: value
  }
  dispatch( { type: UPDATED_PREFERENCE, payload: payload } )
}

export const verifyAuth = (callback) => ( dispatch ) => {
  firebase.auth().onAuthStateChanged( user => {
    if ( user ) {
      dispatch( { type: ADD_USER_ID, payload: user } )
      callback()
    }
  } )
}

export const changePassword = ( payload, callback ) => ( dispatch ) => {
  var user = firebase.auth().currentUser
  if ( payload.newPassword !== payload.confirmNewPassword ) {
    dispatch( { type: CHANGE_PASSWORD_FAILURE, payload: "error.message" } )
  }
  else {
    const credential = firebase.auth.EmailAuthProvider.credential(
      user.email,
      payload.currentPassword
    )
    user.reauthenticateWithCredential( credential ).then( () => {
      user.updatePassword( payload.newPassword )
        .then( () => {
          dispatch( { type: CHANGE_PASSWORD_SUCCESS, payload: true } )
          const credential2 = firebase.auth.EmailAuthProvider.credential(
            user.email,
            payload.newPassword
          )
          user.reauthenticateWithCredential( credential2 )
            .then( () => {
              //  signOut(dispatch)
              // callback()
            } )
            .catch( ( error ) => {
              dispatch( { type: CHANGE_PASSWORD_FAILURE, payload: error.message } )
            } )
        } ).catch( ( error ) => {
        } )
    } )
  }
}

export const resetPassword = ( email ) => ( dispatch ) => {
  firebase
    .auth()
    .sendPasswordResetEmail( email )
    .then( () => {
      dispatch( { type: RESETPASS_USER_SUCCESS, payload: true } )
    } )
    .catch( ( error ) => {
      dispatch( { type: RESETPASS_USER_FAILURE, payload: error.message } )
    } )
}
export const resetMessage = () => ( dispatch ) => {
  dispatch( { type: RESETMESSAGE_ERROR_MESSAGE, payload: null } )
}
