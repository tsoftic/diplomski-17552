import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { resetPassword,resetMessage } from '../actions'
import ForgotPassword from '../components/forgotPassword/ForgotPassword'

class ForgotPasswordContainer extends Component {

  constructor( props ) {
    super( props )
  }

  render() {
    return (
      <ForgotPassword resetProps = { this.props } />
    )
  }
}

const mapStateToProps = ( { authState } ) => ( {
  uid: authState.uid,
  message: authState.message,
  linkSent: authState.linkSent,
  resetMessageAction: authState.message
} )

const enhance = compose( connect( mapStateToProps, { resetPassword,resetMessage } ) )

export default enhance( ForgotPasswordContainer )
