import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { signOut, authenticate, authWithEmailPassword,resetMessage, loadInquiry} from '../actions'
import Login from '../components/login/Login'

class LoginContainer extends Component {

  constructor( props ) {
    super( props )
    if( window.localStorage.getItem( 'carola' ) )
      this.props.history.push( '/dashboard' )
  }

  render() {
    return (
      <Login loginProps = { this.props } />
    )
  }
}


const mapStateToProps = ( { authState, inquiryState } ) => ( {
  uid: authState.uid,
  message: authState.message,
  resetMessageAction: authState.message,
  inqury: inquiryState.inqury
} )

const enhance = compose( connect( mapStateToProps, { signOut, authenticate, authWithEmailPassword,resetMessage, loadInquiry} ) )

export default enhance( LoginContainer )
