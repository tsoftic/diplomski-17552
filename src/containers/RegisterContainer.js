import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { registerUser, authenticate,resetMessage } from '../actions'
import Register from '../components/register/Register'

class RegisterContainer extends Component {

  constructor( props ) {
    super( props )
    if( window.localStorage.getItem( 'carola' ) )
      this.props.history.push( '/diagnosis' )
  }

  render() {
    return (
      <Register registerProps = { this.props } />
    )
  }
}

const mapStateToProps = ( { authState } ) => ( {
  uid: authState.uid,
  message: authState.message,
  resetMessageAction: authState.message
} )

const enhance = compose( connect( mapStateToProps, { registerUser, authenticate,resetMessage} ) )

export default enhance( RegisterContainer )
