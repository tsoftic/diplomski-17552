import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import {
  irratableBowelSyndrome,
  celiacDisease,
  irratableBowelDisease,
  otherCondition,
  sliderChange,
  save
} from '../actions'
import {
  withFooter
} from '../components/common'

import Diagnosis from '../components/diagnosis/Diagnosis'

class DiagnosisContainer extends Component {

  render() {
    return (
      <Diagnosis { ...this.props } />
    )
  }
}

const mapStateToProps = ( { inquiryState, authState } ) => ( {
  inquiry: inquiryState,
  uid: authState.uid,
  email: authState.email,
  displayName: authState.displayName
} )

const enhance = compose(
  connect( mapStateToProps, {
    irratableBowelSyndrome,
    celiacDisease,
    irratableBowelDisease,
    otherCondition,
    sliderChange,
    save
  } ),
  withFooter()
)

export default enhance( DiagnosisContainer )
