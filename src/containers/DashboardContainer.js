import { connect } from 'react-redux'
import Dashboard from '../components/diet/Dashboard'
import { compose } from 'recompose'
import {
  withHeaderAndFooter
} from '../components/common'

const mapStateToProps = ( { inquiryState, authState } ) => ( {
  inquiry: inquiryState,
  uid: authState.uid,
  email: authState.email,
  displayName: authState.displayName,
  authState,
} )

const enhance = compose(
  connect( mapStateToProps, null ),
  withHeaderAndFooter()
)

export default enhance( Dashboard )
