import { connect } from 'react-redux'
import dayMeals from '../../components/mealPlan/dayMeals'
import { compose } from 'recompose'
import {
  withHeaderAndFooter
} from '../../components/common'

import {
  selectMeal,
  getChallengeWeek
} from '../../actions'

const mapStateToProps = ( { inquiryState, authState, dayMealState },{ match: { params } } ) => {
  return {
    inquiry: inquiryState,
    challengeWeek: dayMealState.challengeWeek,
    uid: authState.uid,
    email: authState.email,
    displayName: authState.displayName,
    params
  }
}

const enhance = compose(
  connect( mapStateToProps, { selectMeal, getChallengeWeek} ),
  withHeaderAndFooter()
)

export default enhance( dayMeals )
