import { connect } from 'react-redux'
import Meal from '../../components/mealPlan/Meal'
import { compose } from 'recompose'
import {
  withHeaderAndFooter
} from '../../components/common'
import {
  selectMeal,
  getChallengeWeek
} from '../../actions'

const mapStateToProps = ( { inquiryState, authState, singleMealState },{ match: { params } }  ) => ( {
  inquiry: inquiryState,
  mealProps: singleMealState.meal,
  uid: authState.uid,
  email: authState.email,
  displayName: authState.displayName,
  params
} )
const enhance = compose(
  connect( mapStateToProps, { selectMeal, getChallengeWeek } ),
  withHeaderAndFooter()
)

export default enhance( Meal )
