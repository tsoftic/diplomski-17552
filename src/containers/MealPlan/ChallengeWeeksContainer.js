import { connect } from 'react-redux'
import challengeWeeks from '../../components/mealPlan/challengeWeeks'
import { compose } from 'recompose'
import {
  withHeaderAndFooter
} from '../../components/common'

import {
  selectMeal,
  getChallengeWeek,
  getAllChallengeWeeks
} from '../../actions'
const mapStateToProps = ( { inquiryState, authState, dayMealState } ) => ( {
  inquiry: inquiryState,
  challengeWeeks: dayMealState.allChallengeWeeks,
  uid: authState.uid,
  email: authState.email,
  displayName: authState.displayName
} )

const enhance = compose(
  connect( mapStateToProps, { selectMeal, getChallengeWeek ,getAllChallengeWeeks } ),
  withHeaderAndFooter()
)

export default enhance( challengeWeeks )
