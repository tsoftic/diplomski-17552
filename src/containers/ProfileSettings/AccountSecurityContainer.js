import { connect } from 'react-redux'
import AccountSecurity from '../../components/profileSettings/accountSecurity'
import { compose } from 'recompose'
import { changePassword } from '../../actions/AuthActions'
import {
  withHeaderAndFooter
} from '../../components/common'

const mapStateToProps = ( { authState } ) => {
  return {
    uid: authState.uid,
    message: authState.message
  }
}
const enhance = compose(
  connect( mapStateToProps, { changePassword } ),
  withHeaderAndFooter()
)

export default enhance( AccountSecurity )
