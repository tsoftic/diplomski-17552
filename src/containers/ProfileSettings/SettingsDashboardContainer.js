import { connect } from 'react-redux'
import SettingsDashboard from '../../components/profileSettings/settingsDashboard'
import { compose } from 'recompose'
import {
  withHeaderAndFooter
} from '../../components/common'

const mapStateToProps = ( { inquiryState, authState } ) => ( {
  inquiry: inquiryState,
  uid: authState.uid,
  email: authState.email,
  displayName: authState.displayName
} )

const enhance = compose(
  connect( mapStateToProps, null ),
  withHeaderAndFooter()
)

export default enhance( SettingsDashboard )
