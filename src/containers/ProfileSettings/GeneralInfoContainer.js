import { connect } from 'react-redux'
import GeneralInfo from '../../components/profileSettings/generalInfo'
import { compose } from 'recompose'
import { updateGeneralInfo, loadProfile } from '../../actions/AuthActions'
import {
  withHeaderAndFooter
} from '../../components/common'

const mapStateToProps = ( { authState } ) => ( {
  uid: authState.uid,
  message: authState.message,
  profileSettings: authState.profileSettings,
} )

const enhance = compose(
  connect( mapStateToProps, { updateGeneralInfo, loadProfile } ),
  withHeaderAndFooter()
)

export default enhance( GeneralInfo )
