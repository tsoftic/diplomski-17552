import { connect } from 'react-redux'
import EmailPreferences from '../../components/profileSettings/emailPreferences'
import { compose } from 'recompose'
import { loadEmailPreferences, updateEmailPreferences, updatePreference } from "../../actions/AuthActions"
import {
  withHeaderAndFooter
} from '../../components/common'

const mapStateToProps = ( { authState } ) => ( {
  uid: authState.uid,
  message: authState.message,
  email: authState.emailPreferences ? authState.emailPreferences.email : '',
  preferences: [
    {
      title: 'Allow Notifications',
      subtitle: 'You will receive emails about Diet Challenge status and newsletters',
      checked: authState.emailPreferences ? authState.emailPreferences.allowNotifications : false,
    }, {
      title: 'Newsletters',
      subtitle: '',
      checked: authState.emailPreferences ? authState.emailPreferences.newsletters : false,
    }, {
      title: 'Diet challenge status',
      subtitle: '',
      checked: authState.emailPreferences ? authState.emailPreferences.dietChallengeStatus : false,
    }
  ]
} )
const enhance = compose(
  connect( mapStateToProps, { loadEmailPreferences, updateEmailPreferences, updatePreference } ),
  withHeaderAndFooter()
)

export default enhance( EmailPreferences )
