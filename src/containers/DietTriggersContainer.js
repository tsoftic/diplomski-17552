import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import DietTriggers from '../components/diet/DietTriggers'
import { insertTrigger, loadTrigger, updateTrigger } from "../actions/dashboardActions"
import {
  withHeaderAndFooter
} from '../components/common'

class DietTriggersContainer extends Component {

  render() {
    return (
      <div className="Main">
        <DietTriggers { ...this.props } />
      </div>
    )
  }
}

const mapStateToProps = ( { dashboardState, authState } ) => ( {
  uid: authState.uid,
  triggers: [
    {
      title: 'Week 1: Pasta',
      subtitle: 'Ex: sphagetti, penne',
      checked: dashboardState.trigger.pasta,
    },
    {
      title: 'Week 2: Meat',
      subtitle: 'Ex: beef, steak',
      checked: dashboardState.trigger.meat,
    }, {
      title: 'Week 3: Seafood',
      subtitle: 'Ex: fish, sushi',
      checked: dashboardState.trigger.seafood,
    },
    {
      title: 'Week 4: Diary Products',
      subtitle: 'Ex: cheese, yogurt',
      checked: dashboardState.trigger.diaryProducts,
    },
    {
      title: 'Week 5: Vegetables',
      subtitle: 'Ex: celery, carrot',
      checked: dashboardState.trigger.vegetables,
    },
    {
      title: 'Week 6: Fruits',
      subtitle: 'Ex: mango, watermelon',
      checked: dashboardState.trigger.fruits,
    },
  ],
  sidebars: [
    {
      greyLinks: true,
      title: 'Trigger FAQ',
      links: [
        {
          id: 0,
          title: 'Learn about Milk Sugar',
          route: '#'
        }, {
          id: 1,
          title: 'Learn about Fruit Sugar',
          route: '#'
        }, {
          id: 2,
          title: 'Learn about Sugar Alchohol (Sorbitol)',
          route: '#'
        }, {
          id: 3,
          title: 'Learn about Sugar Alcohol (Mannitol)',
          route: '#'
        }, {
          id: 4,
          title: 'Learn about Fibers',
          route: '#'
        },
      ]
    }, {
      greyLinks: false,
      title: 'Health Articles',
      links: [
        {
          id: 0,
          title: 'What\'s the intolerance to specific food?',
          route: '#'
        }, {
          id: 1,
          title: 'Can intolerance to specific food affect your health?',
          route: '#'
        }
      ]
    }
  ]
} )

const enhance = compose(
  connect( mapStateToProps, { insertTrigger, loadTrigger, updateTrigger } ),
  withHeaderAndFooter()
)

export default enhance( DietTriggersContainer )
