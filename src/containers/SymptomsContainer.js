import React, { Component } from 'react'
import { connect } from 'react-redux'
import Symptoms from '../components/diet/Symptoms'
import { addDefaultSymptoms } from "../actions/dashboardActions.js"
import { compose } from 'recompose'
import {
  withHeaderAndFooter
} from '../components/common'

class SymptomsContainer extends Component {

  render() {
    return (
      <div className="Main">
        <Symptoms { ...this.props } />
      </div>
    )
  }
}

const mapStateToProps = ( { authState } ) => ( {
  data: true,
  uid: authState.uid
} )

const enhance = compose(
  connect( mapStateToProps,  { addDefaultSymptoms } ),
  withHeaderAndFooter()
)

export default enhance( SymptomsContainer )
