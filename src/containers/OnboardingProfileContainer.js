import { connect } from 'react-redux'
import OnboardingProfile from '../components/onboardingProfile/OnboardingProfile'
import { compose } from 'recompose'
import {
  withFooterOnboarding
} from '../components/common'

const mapStateToProps = ( { inquiryState, authState } ) => ( {
  inquiry: inquiryState,
  uid: authState.uid,
  email: authState.email,
  displayName: authState.displayName
} )

const enhance = compose(
  connect( mapStateToProps, null ),
  withFooterOnboarding()
)

export default enhance( OnboardingProfile )
