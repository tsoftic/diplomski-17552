export const getDate = () => {
  var today = new Date()
  return today
}

export const addDays = ( date, daysToAdd ) => {
  if ( !date ) {
    date = new Date()
  }
  date.setDate( date.getDate() + daysToAdd )
  return date
}

export const addHours = ( date, hoursToAdd ) => {
  var parsedDate = new Date( date )
  parsedDate.setHours( parsedDate.getHours() + hoursToAdd )

  return parsedDate
}

export const addMinutes = ( date, minsToAdd ) => {
  var parsedDate = new Date( date )
  parsedDate.setMinutes( parsedDate.getMinutes() + minsToAdd )
  return parsedDate
}

export const getMaxDate = ( dates ) => {
  var maxDate = new Date( Math.max.apply( null, dates ) )
  return maxDate
}