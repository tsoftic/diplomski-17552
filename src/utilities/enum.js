export const decision = {
  True: "True",
  False: "False"
}

export const diseaseType = {
  Ibs: 'Irritable Bowel Syndrome',
  Cd: 'Celiac Disease',
  Ibd: 'irritableBowelDisease',
}

export const triggerFood = {
  Pasta: 'Pasta',
  Meat: 'Meat',
  Seafood: 'Seafood',
  DiaryProducts: 'Diary Products',
  Vegetables: 'Vegetables',
  Fruits: 'Fruits'
}

export const symptom = {
  OverallCondition: 'Overall Condition',
  AbdominalPain: 'Abdominal pain / Discomfort',
  Bloating: 'Bloating',
  Gas: 'Gas',
  Lethargy: 'Lethargy',
  Nausea: 'Nausea',
  Diarrhea: 'Diarrhea',
  Constipation: 'Constipation',
  StressLevel: 'Stress Level',
  MealOutsideDiet: 'Meal Outside Diet'
}

export const gender = {
  Unknown: 'Unknown',
  Male: 'Male',
  Female: 'Female'
}

export const emailPreference = {
  AllowPreferences: 'Allow Notifications',
  Newsletters: 'Newsletters',
  DietChallengeStatus: 'Diet challenge status'
}
