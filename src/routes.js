import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

import LoginContainer from './containers/LoginContainer'
import ForgotPasswordContainer from './containers/ForgotPasswordContainer'
import RegisterContainer from './containers/RegisterContainer'
import DiagnosisContainer from './containers/DiagnosisContainer'
import DashboardContainer from './containers/DashboardContainer'
import DietTriggersContainer from './containers/DietTriggersContainer'
import SymptomsContainer from './containers/SymptomsContainer'

import OnboardingProfileContainer from './containers/OnboardingProfileContainer'

import AccountSecurityContainer from './containers/ProfileSettings/AccountSecurityContainer'
import EmailPreferencesContainer from './containers/ProfileSettings/EmailPreferencesContainer'
import GeneralInfoContainer from './containers/ProfileSettings/GeneralInfoContainer'
import SettingsDashboardContainer from './containers/ProfileSettings/SettingsDashboardContainer'


import MealContainer from './containers/MealPlan/MealContainer'
import DayMealsContainer  from './containers/MealPlan/DayMealsContainer'
import ChallengeWeeksContainer  from './containers/MealPlan/ChallengeWeeksContainer'


const hasUIDToken = () => {
  const token = JSON.parse(window.localStorage.getItem( 'carola' ))
  if (!!token && !!token.uid){
    
    return true
  }
  else{
    window.localStorage.removeItem('carola')
    return false
  }
}

const PrivateRoute = ( { component: Component, ...rest } ) => (
    hasUIDToken() ? (
      <Route {...rest}
        render={
          (props) =>{ 
            return ( <Component { ...props }/> )}
        }
      />
    ): (<Redirect to="/login" />)
    
)


export default () => {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={ LoginContainer }/> {/* need to be changed */}
        <Route path="/login" component={ LoginContainer }/>
        <Route path="/register" component={ RegisterContainer }/>
        <Route path="/forgotPassword" component={ ForgotPasswordContainer }/>
        <PrivateRoute path="/diagnosis" component={ DiagnosisContainer }/>
        <PrivateRoute path="/onboarding" component={ OnboardingProfileContainer }/>

        <PrivateRoute path="/settings" component={ SettingsDashboardContainer }/>
        <PrivateRoute path="/security" component={ AccountSecurityContainer }/>
        <PrivateRoute path="/emailPreferences" component={ EmailPreferencesContainer }/>
        <PrivateRoute path="/general" component={ GeneralInfoContainer }/>

        <PrivateRoute path="/dashboard" component={ DashboardContainer }/>
        <PrivateRoute path="/triggers" component={ DietTriggersContainer }/>
        <PrivateRoute path="/symptoms" component={ SymptomsContainer }/>


        <PrivateRoute path="/meal/:weektitle/:title/:id" component={ MealContainer }/>
        <PrivateRoute path="/dayMeals/:weektitle/:weekid" component={ DayMealsContainer }/>
        <PrivateRoute path="/mealPlan" component={ ChallengeWeeksContainer }/>

      </Switch>
    </BrowserRouter>
  )
}
