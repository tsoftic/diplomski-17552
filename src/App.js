import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import './App.css'
import Login from './components/login/Login'

class App extends Component {

  render() {
    return (
      <div className="App">
        <Login/>
      </div>
    )
  }
}

const mapStateToProps = ( { testState } ) => ( {
  data: testState.data,
} )

const enhance = compose( connect( mapStateToProps, null ) )

export default enhance( App )
