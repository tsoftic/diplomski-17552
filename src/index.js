import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import * as firebase from 'firebase'
import ReduxThunk from 'redux-thunk'

import AppRoutes from './routes'
import './index.css'
import registerServiceWorker from './registerServiceWorker'
import reducers from './reducers'
import { verifyAuth } from './actions/AuthActions'


// initialize firebase Config
const firebaseConfig = {
  apiKey: "AIzaSyBzDYiemPsYV8MeD93eT5DUcMQ7ZUhRfs8",
  authDomain: "carola-e673d.firebaseapp.com",
  databaseURL: "https://carola-e673d.firebaseio.com",
  projectId: "carola-e673d",
  storageBucket: "carola-e673d.appspot.com",
  messagingSenderId: "577820181598"
}

firebase.initializeApp( firebaseConfig )

const store = createStore( reducers, {}, applyMiddleware( ...[ ReduxThunk ] ) )
store.dispatch( verifyAuth())

ReactDOM.render(
  <Provider store={ store }>
    <AppRoutes />
  </Provider>,
  document.getElementById( 'root' )
)
registerServiceWorker()